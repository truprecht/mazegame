import Data.List.Extra (ppNestedList)
import Data.Maybe (fromMaybe)
import Data.Rand
import System.Random
import Game
import Game.History
import Sim.ModelParam
import Sim.Model
import Control.Arrow (second)
import System.Environment
import qualified System.Console.GetOpt as GetOpt
import qualified System.Posix.Files as F
import qualified System.IO.Strict as SIO


simModel :: ModelType -> ModelParam -> Game -> RandM [History]
simModel mt p = sim (modelInit mt p) StepStart
  where
    sim :: Model Double -> GameStep -> Game -> RandM [History]
    sim m c g = do l <- case gameStep g of
                             StepStop -> return []
                             s | ns s -> do chosenStepDir <- StepDir `fmap` rand ecl
                                            let g' = stepGame g chosenStepDir
                                            sim m' chosenStepDir g'
                               | otherwise -> error ("sim got " ++ show s)
                   return $ History{ histChoice = c, histGame = g, histTime = 0, histChoiceTime = 0 } : l
      where
        (m', vcl) = modelStep m g
        ecl = map (second snd) vcl
        ns StepStart = True
        ns (StepDir _) = True
        ns StepWarp = True
        ns _ = False

data Options = Options{ mazeSeed :: Maybe StdGen
                      , simSeed :: Maybe StdGen
                      , defFile :: Maybe String
                      , dumpFile :: Maybe String
                      , modelParam :: MPof Double
                      , steps :: Int
                      , historyFile :: Maybe FilePath
                      }

optDefinition :: [GetOpt.OptDescr (Options -> Options)]
optDefinition = [ GetOpt.Option "" ["simulation-seed"] (GetOpt.ReqArg (\ str x -> x{ simSeed = Just $ read str }) "<SEED>") ("random seed for maze generation")
                , GetOpt.Option "" ["generation-seed"] (GetOpt.ReqArg (\ str x -> x{ mazeSeed = Just $ read str }) "<SEED>") ("random seed for maze generation")
                , GetOpt.Option "f" ["file"] (GetOpt.ReqArg (\ str x -> x{ defFile = Just str }) "<FILE>") ("read maze form file")
                , GetOpt.Option "d" ["dump-maze"] (GetOpt.ReqArg (\ str x -> x{ dumpFile = Just str }) "<FILE>") ("dump maze as file")
                , GetOpt.Option "p" ["param"] (GetOpt.ReqArg (\ str x -> x{ modelParam = updateFromString (modelParam x) str }) "<PARAM>") ("model parameters")
                , GetOpt.Option "n" ["steps"] (GetOpt.ReqArg (\ str x -> x{ steps = read str }) "<INTEGER>") ("limit steps")
                , GetOpt.Option "a" ["append"] (GetOpt.ReqArg (\ str x -> x{ historyFile = Just str }) "FILE") ("append final play history to FILE")
                ]

defaultOptions :: Options
defaultOptions = Options{ mazeSeed = Nothing
                        , simSeed = Nothing
                        , defFile = Nothing
                        , historyFile = Nothing
                        , dumpFile = Nothing
                        , modelParam = initModelParam
                        , steps = 500
                        }

help :: String -> String
help progname = GetOpt.usageInfo ("Usage: " ++ progname ++ " <MODEL TYPE> [OPTIONS]") optDefinition
                ++ "\nFor further information about these options, please read README.md."

readFile' :: String -> IO String
readFile' "-" = getContents
readFile' s = readFile s

main :: IO ()
main = do prog <- getProgName
          args <- getArgs

          (model, options') <- case args of
                                    (modelstring:args') -> return (read modelstring, args')
                                    _                   -> do putStrLn $ help prog
                                                              error "Invalid arguments."
          options <- case GetOpt.getOpt GetOpt.Permute optDefinition options' of
                          (o, [], []) -> return $ foldl (flip ($)) defaultOptions o
                          _           -> do putStr $ help prog
                                            error "Invalid arguments."
          
          simseed <- fromMaybe newStdGen (return <$> simSeed options)
          genseed <- fromMaybe newStdGen (return <$> mazeSeed options)
          game <- maybe (return $ runRand genseed $ newGame (steps options)) ((read <$>) . readFile') (defFile options)
          
          maybe (return ()) (\ x -> writeFile x $ show game) (dumpFile options)

          let simhist = simseed `runRand` simModel model (modelParam options) game

          case historyFile options of
               Nothing -> putStrLn $ ppNestedList [simhist]
               Just histfile -> do fe <- F.fileExist histfile
                                   previousHistory <- if fe
                                                        then read <$> SIO.readFile histfile
                                                        else return []
                                   writeFile histfile $ ppNestedList (simhist : previousHistory)