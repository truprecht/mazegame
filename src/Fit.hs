{-# LANGUAGE BangPatterns #-}

import Game.History
import Sim.ModelParam
import Sim.Model
import System.Environment
import qualified System.Console.GetOpt as GetOpt
import Control.Monad (when)
import Data.Rand (runSeed)
import qualified System.IO.Strict as SIO

data LRPolicy = InverseIteration | Step Int Double | Constant | Dynamic Int deriving (Show, Read)
data Initials = Default | Specified ModelParam | Random Int

data Options = Options{ lrPol :: LRPolicy
                      , maxIterations :: Int
                      , minImprovement :: Double
                      , baseLR :: Double
                      , baseParam :: Initials
                      , verbose :: Bool
                      }

fitModel :: ModelType -> ModelParam -> [[History]] -> LRPolicy -> Double -> Int -> Double -> [(ModelParam, ModelParam, Double, Double, Double)]
fitModel mt initialParam hss pol lr stopIt minIm 
  = map (\ (p, p', l) -> ( p
                         , p'
                         , l / fromIntegral (length hss)
                         , l + fromIntegral freeparam / 2 + log (fromIntegral observations)
                         , (1 :: Double) - l / l_random
                         ))
  $ fitstep pol initialParam 1 (1 / 0)
    where
      observations :: Int
      observations = sum (map length hss)

      freeparam :: Int
      freeparam = case mt of
                      ModelDumb    -> 0
                      ModelFull    -> 7
                      ModelQLA     -> 7
                      ModelFP      -> 7
                      ModelRASR    -> 5
                      ModelRESucc  -> 5
                      ModelTD      -> 5

      l_random :: Double
      l_random = fst $ decisionLikelihood hss ModelDumb (pure 0)

      mask_zero :: ModelParam -> ModelParam
      mask_zero mp = fromList masked_list
        where 
          [gamma, h, door, alpha, q0, lambda, beta, depth] = toList mp
          masked_list
            | mt == ModelDumb = replicate 8 0
            | mt == ModelTD = [gamma, 0, 0, alpha, q0, lambda, beta, 0]
            | mt == ModelQLA = [gamma, h, 0, 0, 0, 0, beta, 0]

      lrpoliciy :: LRPolicy -> Int -> Double -> Double
      lrpoliciy InverseIteration iteration baselr = baselr / fromIntegral iteration
      lrpoliciy (Step stepsize gamma) iteration baselr = baselr / (gamma ^ (floor (fromIntegral stepsize / fromIntegral iteration :: Double) :: Int))
      lrpoliciy Constant _ baselr = baselr
      lrpoliciy (Dynamic modifier) _ baselr = baselr * (2 ^^ (- div modifier 10))

      fitstep :: LRPolicy -> ModelParam -> Int -> Double -> [(ModelParam, ModelParam, Double)]
      fitstep policy p it previousLikelihood
        | it > stopIt = []
        | previousLikelihood - likelihood < minIm = case policy of
                                                         Constant -> []
                                                         InverseIteration -> fitstep policy p (it+1) previousLikelihood
                                                         (Step stepsize _) -> fitstep policy p (stepsize * (1 + div it stepsize)) previousLikelihood
                                                         (Dynamic modifier) -> if modifier < 100 
                                                                                 then fitstep (Dynamic $! modifier + 10) p (it+1) previousLikelihood
                                                                                 else []
        | otherwise = (p, gradient, likelihood) : fitstep nextpol (proper nextParam) (it+1) likelihood
          where
            !(!likelihood, !gradient_raw) = decisionLikelihood hss mt p
            !steplr = lrpoliciy policy it lr
            !gradient_magnitude = sqrt (sum ((^ (2 :: Int)) <$> mask_zero gradient_raw))
            !gradient = if gradient_magnitude > 0
                          then (/ gradient_magnitude) <$> mask_zero gradient_raw
                          else pure 0
            !nextParam = ((+) . negate . (*) steplr) <$> gradient <*> p
            nextpol = case policy of
                           (Dynamic modifier) -> Dynamic $! (modifier - 1)
                           _ -> policy


help :: String -> String
help progname = GetOpt.usageInfo ("Usage: " ++ progname ++ " <HISTORY FILE> <MODEL TYPE> [OPTIONS]") optDefinitions
                ++ "\nFor further information about these options, please read README.md."

defaultOptions :: Options
defaultOptions = Options{ lrPol = Dynamic 0
                        , maxIterations = 100
                        , minImprovement = 1e-4
                        , baseLR = 0.01
                        , baseParam = Default
                        , verbose = False
                        }

optDefinitions :: [GetOpt.OptDescr (Options -> Options)]
optDefinitions = [ GetOpt.Option "v" ["verbose"]      (GetOpt.NoArg   (\ x -> x{ verbose = True }))                                                  ("output all iteration's results")
                 , GetOpt.Option ""  ["policy"]       (GetOpt.ReqArg  (\ str x -> x{ lrPol = read str }) "<POLICY>")                                 ("define learning rate policy")
                 , GetOpt.Option "i" ["iterations"]   (GetOpt.ReqArg  (\ str x -> x{ maxIterations = read str }) "<INTEGER>")                        ("max nr. of iterations")
                 , GetOpt.Option ""  ["improvement"]  (GetOpt.ReqArg  (\ str x -> x{ minImprovement = read str }) "<FLOATING>")                      ("minimal improvement of likelihood")
                 , GetOpt.Option "l" ["learningrate"] (GetOpt.ReqArg  (\ str x -> x{ baseLR = read str }) "<FLOATING>")                              ("define the base learning rate")
                 , GetOpt.Option "p" ["parameters"]   (GetOpt.ReqArg  (\ str x -> x{ baseParam = case baseParam x of
                                                                                                      (Specified bp) -> Specified $ updateFromString bp str
                                                                                                      (_) -> Specified $ updateFromString initModelParam str }) "<PARAMDEF>") ("define base parameters for the model")
                 , GetOpt.Option "r" ["random"]       (GetOpt.ReqArg  (\ str x -> x{ baseParam = Random $ read str }) "<INTEGER>")                   ("generate base parameters for the model")
                 ]

tableColumns :: (Show a, Show b, Show c, Show d, Show e) => (a,b,c,d,e) -> String
tableColumns (v,w,x,y,z) = show v ++ "\t" ++ show w ++ "\t" ++ show x ++ "\t" ++ show y ++ "\t" ++ show z

main :: IO()
main = do prog <- getProgName
          args <- getArgs
          
          (historyFile, model, options') <- case args of
                                                 (hf:mt:args') -> return (hf, read mt, args')
                                                 _             -> do putStrLn $ help prog
                                                                     error "Missing arguments."
          options <- case GetOpt.getOpt GetOpt.Permute optDefinitions options' of
                          (o, [], []) -> return $ foldl (flip ($)) defaultOptions o
                          _           -> do putStr $ help prog
                                            error "Invalid arguments."
          
          history <- read <$> case historyFile of
                                   "-" -> SIO.getContents
                                   _   -> SIO.readFile historyFile
          when (null history) (error "Empty history file.")

          let baseParam' = case baseParam options of
                                (Default) -> inits model
                                (Specified bp) -> bp
                                (Random seed) -> runSeed seed randomParam
              learningSequence = fitModel model baseParam' history (lrPol options) (baseLR options) (maxIterations options) (minImprovement options)
          
          let randomLikelihood = fst (decisionLikelihood history ModelDumb (pure 0))
          if verbose options
            then putStrLn $ "Fitting " ++ show model
                            ++ " to " ++ show (length history) ++ " subjects (" ++ show (sum (map length history)) ++ " steps in total).\n"
                            ++ "Random likelihood: " ++ show randomLikelihood
            else return ()

          if verbose options
             then putStrLn (tableColumns ("current parameters", "gradient", "likelihood per subject", "BIC", "rho^2"))
                  >> (putStrLn $ unlines $ map tableColumns learningSequence)
             else putStrLn (tableColumns ("current parameters", "random", "likelihood per subject", "BIC", "rho^2"))
                  >> (putStrLn $ tableColumns $ (\ (a, _, c, d, e) -> (a, randomLikelihood, c, d, e)) $ last learningSequence)