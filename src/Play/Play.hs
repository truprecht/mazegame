module Play.Play (
    PlayMode(..),
    PlayOptions(..),
    play
  ) where

import System.IO
import Control.Monad
import Graphics.UI.GLUT
import Data.Maybe
import Data.IORef
import Play.Space
import Maze.World
import Play.Render
import Play.Render.Display
import Data.Rand
import Play.State
import Game
import Game.History
#ifdef MAZE_PROC
import Sim.Model
import Sim.ModelParam
import Play.Console
#endif
import Play.Break
import Control.Arrow (second)

data PlayMode = PlayTest | PlayReal | PlayScan deriving (Eq, Show)

data PlayOptions = PlayOptions {
    playMode :: PlayMode,
    playFS :: Bool,
    playDisplay :: DisMode,
    playEvents :: Maybe Handle,
    playVerbose :: Bool
  }

play :: PlayOptions -> Game -> IO [History]
play opts initgame
  = do inittime <- time
       initdur <- runStdRand $ stanceDuration StanceStand
       curr <- newIORef State{ stateGame = initgame
                             , stateTime = inittime
                             , stateStance = StanceStand
                             , stateNextTime = inittime + initdur
                             , stateEdgeValues = Nothing
                             }
       history <- newIORef []
#ifdef MAZE_PROC
       modparam <- newIORef initModelParam
       modtype <- newIORef ModelDumb
       sim <- newIORef False
       modev <- newIORef False
#endif
       let setGame g = modifyIORef curr (\c -> c{ stateGame = g })
           addhist h = do modifyIORef history (h:)
                          maybe (return ()) (`hPrint` h) (playEvents opts)
#ifdef MAZE_PROC
           modeval = do mt <- readIORef modtype
                        mp <- readIORef modparam
                        h <- readIORef history
                        return $ modelRevHist mt mp (reverse h)
#endif
           stance s e = do cur <- readIORef curr
                           t <- time
                           d <- runStdRand $ stanceDuration s
                           let cur' = cur{ stateStance = s
                                         , stateTime = t
                                         , stateNextTime = t + d
                                         }
                               g = stateGame cur
                           writeIORef curr cur'
                           setDisplay (playDisplay opts) cur' e
                           when (playVerbose opts) $ hPutStr stderr ("\r" ++ show (gameTotalSteps g) ++ "/" ++ show (gameStopSteps g) ++ " " ++ show s ++ " " ++ show d ++ "\ESC[K")

#ifdef MAZE_PROC
           stand = do ev <- readIORef modev
                      when ev 
                        $ do ef <- snd <$> modeval
                             modifyIORef curr (\c -> c{ stateEdgeValues = Just ef })
                      s <- readIORef sim
                      if s
                        then do v <- fst <$> modeval
                                let p = map (second snd) v
                                c <- runStdRand $ rand p
                                choose (StepDir c)
                        else stance StanceStand $ choose StepTimeout
#else
           stand = stance StanceStand $ choose StepTimeout
#endif
                              
           enter = stance StanceEnter stand
           choose ch = do cur <- readIORef curr
                          t <- time
                          let g = stepGame (stateGame cur) ch
                          addhist History{ histTime = stateTime cur
                                         , histChoice = ch
                                         , histChoiceTime = t
                                         , histGame = g 
                                         }
                          if gameStep g /= StepBad 
                             && stateStance cur == StanceStand 
                             && ch /= StepStop && takeBreak (stateGame cur)
                            then addhist (HistoryBreak t) >> stance (if playMode opts == PlayScan then StanceBlank else StanceBreak Nothing) (step g)
                            else step g
           step g = case gameStep g of
                         StepStart -> setGame g >> enter
                         StepStop -> done
                         StepBad -> setGame g >> return ()
                         StepTimeout -> setGame g >> stance StanceTimeout stand
                         StepWarp -> stance (StanceWarp (gamePos g)) $ setGame g >> stand
                         StepDir _ -> stance (StanceTurn (gameDir g)) $ setGame g >> enter
                         _ -> return ()
           input _ Up _ _ = return ()
           input k Down _ _ = do cur <- readIORef curr
                                 t <- time
                                 case k of 
                                      Char c -> addhist (HistoryKeyPress t c)
                                      _ -> return ()
                                 let mov d pf af = case stateStance cur of
                                                        StanceFree p a -> stance (StanceFree (pf a p) (af a)) (return ())
                                                        StanceStand -> choose $ case playDisplay opts of { Display2D -> StepDir (North >+ d) ; Display3D -> StepTurn d }
                                                        StanceBreak Nothing -> when (playMode opts == PlayTest || t > stateTime cur + 5) $ nextDisplay >> return ()
                                                        _ -> return ()
                                 case (playMode opts, k) of
                                      (_, Char 'Q') -> putStrLn "Quit!" >> choose StepStop
                                      (PlayTest, Char 'q') -> choose StepStop
                                      (PlayTest, Char 'n') -> choose StepWarp
                                      (PlayTest, Char 'f') -> do
                                        putStrLn "Entering free motion mode"
                                        stance (StanceFree (Point2 0 0) (ofRadians (0 :: Double))) (return ())
#ifdef MAZE_PROC
                                      (PlayTest, Char 'p') -> readIORef modparam >>= askMP >>= writeIORef modparam
                                      (PlayTest, Char 'm') -> readIORef modtype >>= askFor "Model type:" >>= writeIORef modtype
                                      --(PlayTest, Char 'e') -> modeval >>= print . fst
                                      (PlayTest, Char 's') -> modifyIORef sim not
                                      (PlayTest, Char 'v') -> modifyIORef curr (\c -> c{ stateEdgeValues = Nothing }) >> modifyIORef modev not
#endif
                                      (_, SpecialKey KeyRight) -> mov turnRight (flip const) (ofRadians . (+) (0.1 :: Double) . radians)
                                      (_, SpecialKey KeyLeft) -> mov turnLeft (flip const) (ofRadians . subtract (0.1 :: Double) . radians)
                                      (_, SpecialKey KeyUp) -> mov turnAhead (\a -> (.+^ 0.1*^angle a)) id
                                      (_, SpecialKey KeyDown) -> mov turnBack (\a -> (.-^ 0.1*^angle a)) id
                                      (_, Char '4') -> mov turnLeft (flip const) id
                                      (_, Char '7') -> mov turnLeft (flip const) id
                                      (_, Char '3') -> mov turnAhead (flip const) id
                                      (_, Char '8') -> mov turnAhead (flip const) id
                                      (_, Char '2') -> mov turnRight (flip const) id
                                      (_, Char '9') -> mov turnRight (flip const) id
                                      (_, Char '`') -> do addhist (HistoryBacktick t)
                                                          case stateStance cur of
                                                               StanceBreak (Just n) 
                                                                | n > 0 -> stance (StanceBreak (Just (pred n))) =<< fromJust <$> getNextDisplay
                                                               _ -> return ()
                                      (PlayScan, Char 'c') -> when (stateStance cur == StanceBlank)
                                        $ do stance (StanceBreak (Just 3)) =<< fromJust <$> getNextDisplay
                                      _ -> return ()

           done = leaveMainLoop
       
       openRender (if playFS opts then Just False else Nothing)
        $ do perWindowKeyRepeat $= PerWindowKeyRepeatOff
             when (playMode opts /= PlayTest)
              $ do globalKeyRepeat $= GlobalKeyRepeatOff 
                   cursor $= None
             keyboardMouseCallback $= Just input
             initDisplay
             choose StepStart
       reverse <$> readIORef history
