{-# LANGUAGE ImpredicativeTypes #-}

module Play.Console (
    askFor,
    askMP
  ) where

import System.Console.Readline
import Control.Exception as Exception
import Sim.ModelParam

askFor :: (Read a, Show a) => String -> a -> IO a
askFor p v = maybe v id <$> askForMaybe p (Just v)

askForMaybe :: (Read a, Show a) => String -> Maybe a -> IO (Maybe a)
askForMaybe p v
  = do setPreInputHook (Just $ maybe (return ()) (insertText . show) v >> redisplay)
       i <- readline (p ++ "=")
       case i of
            Nothing -> return Nothing
            Just "" -> return Nothing
            Just i' -> Exception.catch (return $! Just $! read i') (\(Exception.ErrorCall e) -> putStrLn e >> return Nothing)

askMP :: MP -> IO MP
askMP p 
  = do putStrLn "MP:"
       paramList <- mapM (uncurry askFor) 
                    $ toList ((,) <$> mpNames <*> p)
       return $ fromList paramList
