module Play.Break (
    takeBreak,
    breakMode
  ) where

import Data.Maybe
import Game
import Game.Param

breakMode :: Maybe Int -> Game -> Maybe [String]
breakMode Nothing Game{ gameTotalSteps = 0 } = Just ["START", "", "Use arrow keys to move.", "", "Press a key", "when you're ready", "to begin...", "", "Good Luck!"]
breakMode Nothing Game{ gameStopSteps = 0 } = Just ["COMPLETE", "", "You're all done!"]
breakMode Nothing Game{ gameStopSteps = n }
  | n `mod` breakSteps == 0 = Just ["BREAK", "", show (n `div` breakSteps) ++ " more round" ++ if n == breakSteps then "" else "s", "", "Press a key", "when you're ready", "to continue..."]
  | otherwise = Nothing
breakMode (Just w) Game{ gameTotalSteps = t, gameStopSteps = n }
  | n == 0 = Just ["DONE", "", "The final scan will complete shortly."]
  | t == 0 && n > 1000 = Just ["PRACTICE", "", "This round is practice.", "Use pointer/middle/ring fingers to move.", "", "Get ready!", if w < 3 then show (succ w) ++ "..." else ""]
  | t == 0 || n `mod` breakSteps == 0 = Just ["READY", "", show ((n + breakSteps - 1) `div` breakSteps) ++ " more round" ++ if n == breakSteps then "" else "s", "", "Get ready!", if w < 3 then show (succ w) ++ "..." else ""]
breakMode (Just _) Game{} = Nothing

takeBreak :: Game -> Bool
takeBreak = isJust . breakMode Nothing