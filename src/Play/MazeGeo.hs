module Play.MazeGeo (
    direction,
    dirVector,
    wallDir,
    roomSize,
    roomCenter,
    roomCorners,
    mazeCorners
  ) where

import Play.Space
import Maze.World
import Maze.Room
 
roomSize :: Double
roomSize = 2

direction :: Dir -> Angle
direction = vectorAngle . dirVector

dirVector :: Dir -> Vector2 Double
dirVector d = vector2 (fromIntegral x) (fromIntegral y) where (x,y) = dir d

roomCenter :: Room -> Point2 Double
roomCenter (x,y) = Point2 (roomSize * fromIntegral x) (roomSize * fromIntegral y)

wallDir :: Dir -> Vector2 Double
wallDir d = (roomSize/2) *^ dirVector d

roomCorners :: Room -> (Point2 Double, Point2 Double)
roomCorners r = rc .-+^ ed where
  rc = roomCenter r
  ed = wallDir North ^+^ wallDir East

mazeCorners :: (Point2 Double, Point2 Double)
mazeCorners = (fst $ roomCorners r0, snd $ roomCorners r1) where
  (r0, r1) = roomRange

