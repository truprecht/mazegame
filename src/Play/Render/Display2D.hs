module Play.Render.Display2D (
    display2D
  ) where

import Control.Monad
import Graphics.UI.GLUT hiding (Angle)
import Play.Space
import Maze.World
import Maze.Room
import Play.State
import Game
import Play.Render
import Play.Render.Draw
import Play.MazeGeo
import Play.Render.DrawMap

type ViewPos = (Point2 Double, Angle)

standPoint :: Double
standPoint = 0.8

viewPosEnter :: (Room, Dir) -> Float -> ViewPos
viewPosEnter (r,d) t =
  (roomCenter r .-^ ((2-standPoint,standPoint) -@~ (t*(2-t))) *^ wallDir d, direction d)

standPos :: (Room, Dir) -> Point2 Double
standPos (r,d) = roomCenter r .-^ standPoint *^ wallDir d

standDir :: (Room, Dir) -> Angle
standDir (_,d) = direction d

viewPosStand :: (Room, Dir) -> ViewPos
viewPosStand rd = (standPos rd, standDir rd)

viewPosTurn :: (Room, Dir) -> Dir -> Float -> ViewPos
viewPosTurn (r,d1) d2 t = (roomCenter r .+^ standPoint *^ mp, md) where
  a = (0, pi/2) -@~ t
  ed = negateVector $ wallDir d1
  ed' = wallDir d2
  mp = (1 - sin a) *^ ed ^+^ (1 - cos a) *^ ed'
  md = (direction d1, direction d2) -@~ t

drawHere :: Point -> Angle -> Angle -> IO ()
drawHere p a va = unsafePreservingMatrix 
  $ do translate $ glV3 $ p .-. origin
       rotate (degrees a) (Vector3 0 0 (-1 :: Float))
       rotate (degrees va) (Vector3 (1 :: Float) 0 0)
       scale 0.4 0.4 (1 :: Float)
       translate (Vector3 0 0.4 (0 :: Float))
       setSurface $ SurfColor 1 1 1
       drawRect (map (point23 0 . uncurry Point2) [(-1,-1),(0,-0.5),(1,-1),(0,1)]) (vector3 0 0 1)

rewardTexture :: Reward -> TextureType
rewardTexture r
  | r > 0 = TextureWin
  | otherwise = TextureLose

drawReward :: Room -> Float -> Reward -> IO ()
drawReward r t rr = do
  setSurface (SurfTexture 1 TextureChest)
  drawRect (map (point23 (-0.2)) $ b (s/sqrt 2)) n
  when (t > 0) $ do
    setSurface (SurfTexture 1 (rewardTexture rr))
    drawRect (map (point23 (-0.1)) $ b (realToFrac t * s)) n
  where 
    b s' = uncurry rectBounds $ roomCenter r .-+^ (s' * roomSize) *^ vector2 1 1
    s = 0.4
    n = vector3 0 0 1

drawTimeout :: Float -> IO ()
drawTimeout _ = unsafePreservingMatrix $ do
  loadIdentity
  setSurface (SurfColor 1 0 0)
  drawRect (map (point23 0.4) viewSquare) (vector3 0 0 1)
  setSurface (SurfColor 0 0 0)
  drawText (Point3 0 0 0.5) ["TIMEOUT"]

display2D :: State -> IO ()
display2D state@State {
    stateGame = Game {
      gameMaze = m,
      gamePos = p@(r,_),
      gameReward = rr
    },
    stateStance = s,
    stateTime = st0,
    stateEdgeValues = ev
  } = do t <- time
         let st = (st0, stateNextTime state) ~@- t
             (xy,a) = case s of
                           StanceEnter -> viewPosEnter p st
                           StanceTurn dt -> viewPosTurn p dt st
                           StanceFree p' a' -> (p', a')
                           _ -> viewPosStand p
         translate $ glV3 $ vector23 0 $ origin .-. Point2 (ds/2) (ds/2)
         scale (ds/vector2X rs) (ds/vector2Y rs) 1
         translate $ glV3 $ vector23 0 $ origin .-. r0

         drawMap m r
         maybe (return ()) (drawReward r (if s == StanceEnter then 0 else if s == StanceStand then min 1 (2*(t-st0)) else 1)) rr
         case s of 
              StanceWarp p2 -> do
                when (st < 0.75) $ drawHere (point23 0 xy)  a  (ofRadians ((pi/2 :: Float) * ((0,0.75) ~@- st)))
                when (st > 0.25) $ drawHere (point23 0 xy2) a2 (ofRadians ((pi/2 :: Float) * ((1,0.25) ~@- st)))
                  where (xy2,a2) = viewPosStand p2
              _ -> drawHere (point23 0 xy) a 0
         maybe (return ()) (drawEdgeValues) ev
         when (s == StanceTimeout) $ drawTimeout st

    where 
      (r0, r1) = mazeCorners
      rs = r1 .-. r0
      ds = 1.9
