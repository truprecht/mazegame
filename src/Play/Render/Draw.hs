module Play.Render.Draw (
    initDraw,
    rectBounds,
    unitSquare, viewSquare,
    vertRectBounds,
    TextureType(..),
    Surface(..),
    setSurface,
    drawRect,
    drawText,
    drawBzzt,
    drawBreak
  ) where

import Control.Monad
import Data.Array
import Graphics.UI.GLUT
import Play.Space
import Play.Render (orthoRez, withStateVal, viewOrtho, render)
import Data.Maybe (fromMaybe)

rectBounds :: Point2 Double -> Point2 Double -> [Point2 Double]
rectBounds (Point2 x1 y1) (Point2 x2 y2) = [Point2 x1 y1, Point2 x2 y1, Point2 x2 y2, Point2 x1 y2]

viewSquare, unitSquare, unitSquare' :: [Point2 Double]
unitSquare = rectBounds (Point2 0 0) (Point2 1 1)
unitSquare' = [Point2 0 1, Point2 1 1, Point2 1 0, Point2 0 (0 :: Double)]
viewSquare = rectBounds (Point2 (-1) (-1)) (Point2 1 1)

vertRectBounds :: (Point2 Double, Point2 Double) -> (Double, Double) -> [Point]
vertRectBounds (p1, p2) (z1,z2) = [point23 z1 p1, point23 z1 p2, point23 z2 p2, point23 z2 p1]


data TextureType = TextureGround | TextureWall | TextureInnerWall | TextureDoor | TextureSignOut | TextureSignIn | TextureSignHide | TextureChest | TextureWin | TextureLose | TextureBG | Texture1 | Texture2 | Texture3 | Texture4 deriving (Eq, Ord, Bounded, Enum, Ix)


initDraw :: IO ()
initDraw = do
  textureFunction $= Modulate
  colorMaterial $= Just (Front, AmbientAndDiffuse)
  alphaFunc $= Just (Greater, 0.1)
  blendFunc $= (SrcAlpha, OneMinusSrcAlpha)
  lineWidth $= 2

data Surface = SurfColor !Float !Float !Float
             | SurfTexture !Float !TextureType

setSurface :: Surface -> IO ()
setSurface (SurfColor r g b) = do
  texture Texture2D $= Disabled
  lightModelAmbient $= Color4 0 0 0 1
  color $ Color3 r g b
setSurface (SurfTexture s tex) = do
  withStateVal matrixMode Texture $ do
    loadIdentity
    scale s s 1
  let (i, r, g, b)= case tex of 
                         TextureWall -> (0.7, 0, 0, 0) 
                         TextureGround -> (0.5, 0, 0, 0) 
                         TextureLose -> (1, 0.5, 0.5, 0.5) 
                         TextureWin -> (1, 0.5, 0.5, 0.5) 
                         TextureSignIn -> (1, 0.8, 0, 0) 
                         TextureSignOut -> (1, 0.2, 0.2, 0.2) 
                         TextureSignHide -> (1, 0.2, 0.2, 0.2) 
                         _ -> (1, 0, 0, 0) :: (Float, Float, Float, Float)
  lightModelAmbient $= Color4 r g b 1
  color $ Color4 i i i 1

drawRect :: [Point] -> Vector -> IO ()
drawRect v n = do normal (vector3app Normal3 n)
                  unsafeRenderPrimitive Quads $ zipWithM_ vert v unitSquare'
  where
    vert v' t = texCoord (point2app TexCoord2 t) >> vertex (glP3 v')

font :: BitmapFont
font = Helvetica18

drawTextAt :: Point -> String -> IO ()
drawTextAt p s = do
  rasterPos $ glP3 p
  renderString font s

drawTextCenter :: Point -> String -> IO ()
drawTextCenter p s = do
  w <- stringWidth font s
  h <- fontHeight font
  r <- orthoRez
  let p0 = p .-^ vector3 (fromIntegral w/r) (realToFrac h/r/2) 0
  drawTextAt p0 s

drawText :: Point -> [String] -> IO ()
drawText p s = do h <- fontHeight font
                  r <- orthoRez
                  let pi' i = p .-^ vector3 0 (fromIntegral (2 * i - n + 1) * realToFrac h / r) 0
                  mapM_ (\i -> drawTextCenter (pi' i) (s !! i)) [0..pred n]
  where
    n = length s

drawBreak :: Maybe [String] -> IO ()
drawBreak ms = do _ <- viewOrtho
                  setSurface (SurfColor 1 1 1)
                  render
                  drawText origin $ fromMaybe ["Press a key", "to continue..."] ms

drawBzzt :: IO ()
drawBzzt = do _ <- viewOrtho
              setSurface (SurfColor 1 0 0)
              drawRect (map (point23 0.4) $ rectBounds (Point2 (-1) (-0.5)) (Point2 1 0.5)) (vector3 0 0 1)