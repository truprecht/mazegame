module Play.Render.DrawReward (
    drawRoomReward,
    drawReward
  ) where

import Control.Monad
import Data.Maybe
import Graphics.UI.GLUT
import Data.Rand
import Play.Space
import Maze.World
import Maze.Room
import Maze
import Play.Render
import Play.Render.Draw
import Play.MazeGeo
import Game.Param (rewardParams, rewardMult)

rewardColors :: [(RewardParam, Color3 Float)]
rewardColors = zip rewardParams $ runSeed 0 $ randShuffle [Color3 1 0 1, Color3 1 0.5 0, Color3 0.5 1 0, Color3 0 1 1]

rewardColor :: RewardParam -> Color3 Float
rewardColor p = maybe (Color3 0 0 0) id $ lookup p rewardColors

rewardLabels :: [(RewardParam, TextureType)]
rewardLabels = zip rewardParams [Texture2, Texture2, Texture3, Texture3]

rewardLabel :: RewardParam -> TextureType
rewardLabel p = fromJust $ lookup p rewardLabels

drawRoomReward :: Maze -> Room -> Float -> IO ()
drawRoomReward m r open = 
  flip (maybe (return ())) (mazeReward m r) $ \rp -> do
    setSurface (SurfTexture 1 TextureChest)
    color (rewardColor rp)
    withDoubleSided $ do
      b <- mapM side (reverse directions)
      drawRect (map (point23 0.01) b) (vector3 0 0 1)
    unsafePreservingMatrix $ do
      setSurface (SurfTexture 1 (rewardLabel rp))
      color (rewardColor rp)
      translate $ glV3 $ point23 1 (roomCenter r) .-. origin
      scale 0.5 0.5 (0.5 :: Float)
      drawRect (vertRectBounds (Point2 0 0, Point2 1 0) (0,1)) (vector3 0 (-1) 0)
      drawRect (vertRectBounds (Point2 1 0, Point2 0 0) (0,1)) (vector3 0 1 0)
      drawRect (vertRectBounds (Point2 0 0, Point2 0 1) (0,1)) (vector3 1 0 0)
      drawRect (vertRectBounds (Point2 0 1, Point2 0 0) (0,1)) (vector3 (-1) 0 0)
      drawRect (vertRectBounds (Point2 0 0, Point2 (-1) 0) (0,1)) (vector3 0 1 0)
      drawRect (vertRectBounds (Point2 (-1) 0, Point2 0 0) (0,1)) (vector3 0 (-1) 0)
      drawRect (vertRectBounds (Point2 0 0, Point2 0 (-1)) (0,1)) (vector3 (-1) 0 0)
      drawRect (vertRectBounds (Point2 0 (-1), Point2 0 0) (0,1)) (vector3 1 0 0)
  where
    side d = do
      drawRect (vertRectBounds (p2, p1) (0,height)) (vector23 0 out)
      normal $ vector3app Normal3 $ vector23 (cos angle') $ sin angle' *^ out
      unsafeRenderPrimitive Triangles
        $ do texCoord $ TexCoord2 0 (0 :: Float)
             vertex $ glP3 $ point23 height p2
             texCoord $ TexCoord2 1 (0 :: Float)
             vertex $ glP3 $ point23 height p1
             texCoord $ TexCoord2 (1/2) (1/2 :: Float)
             vertex $ glP3 $ point23 (height + radius*sin angle') $ pc .-^ (radius * cos angle') *^ out
      return p1
      where
        d1 = dirVector d
        d2 = dirVector $ succ' d
        out = (d1 ^+^ d2) ^/ sqrt 2
        p1 = roomCenter r .+^ diagonal*^d1
        p2 = roomCenter r .+^ diagonal*^d2
        pc = midpoint p1 p2
    height = 0.2
    diagonal = 0.2
    radius = diagonal/sqrt 2
    angle' = (0,1.5 :: Double) -@~ open

rewardTexture :: Reward -> Face -> TextureType
rewardTexture r _ 
  | r > 0 = TextureWin
  | otherwise = TextureLose

rewardIndex :: Reward -> Int
rewardIndex 0 = 0
rewardIndex 1 = 1
--rewardIndex x = fromJust $ elemIndex x rewardParams -- 5*x
rewardIndex x = round $ rewardMult*x

rewardCount :: Reward -> Int
rewardCount = rewardIndex

drawReward :: Maze -> Room -> Float -> Reward -> IO ()
drawReward _ r rt rr = withStateVal (materialSpecular Front) (Color4 0.8 0.8 0.7 1) 
                     $ withStateVal (materialShininess Front) 5 
                     $ do t <- time
                          let spin = 3*t
                              height = 0.5 * realToFrac rt
                              size = 0.15
                              dir' = angle $ ofRadians spin
                              out = angle $ ofRadians $ spin+pi/2
                              rc = rewardCount rr
                          forM_ [0..pred rc] $ \ri -> do
                          let h = height + (size+0.01)*(0.5 * fromIntegral rc - fromIntegral ri)
                          setSurface (SurfTexture 1 (rewardTexture rr Front))
                          drawRect (vertRectBounds (roomCenter r .+^ 0.001*^out .-+^ (size/2)*^dir') (h, h+size)) (vector23 0 out)
                          setSurface (SurfTexture 1 (rewardTexture rr Back))
                          drawRect (vertRectBounds (roomCenter r .-^ 0.001*^out .-+^ (-size/2)*^dir') (h, h+size)) (vector23 0 $ negateVector out)
