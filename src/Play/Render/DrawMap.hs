{-# LANGUAGE FlexibleContexts #-}

module Play.Render.DrawMap (
    drawMap,
    drawEdgeValues
  ) where

import Graphics.UI.GLUT as GL
import Data.Array
import Play.Space
import Maze.World
import Maze.Room
import Maze.Edge
import Maze
import Play.MazeGeo
import Play.Render
import Play.Render.Draw

drawRoom :: Maze -> Room -> IO ()
drawRoom m r = mapM_ (\d -> dw d $ mazeDoor m (Edge r d)) directions 
  where
    dw d dr = setSurface (s dr)
            >> withStateVal lineWidth 2 
               (unsafeRenderPrimitive Lines 
               $ vertex (glP2 p0) >> vertex (glP2 p1))
        where 
          (p0, p1) = roomCenter r .+^ wallDir d .-+^ wallDir (succ' d)
          s Wall = SurfColor 1 1 1
          s Door = SurfColor 0 0 1
          s DoorInc = SurfColor 0 1 0
          s DoorDec = SurfColor 1 0 0

drawGrid :: IO ()
drawGrid = do
  setSurface (SurfColor 0.75 0.75 0.75)
  unsafeRenderPrimitive Lines
    $ sequence_ [ vrc (i, ry0) >> vrc (i, succ ry1) 
                | i <- [succ rx0 .. rx1]
                ]
    >> sequence_ [ vrc (rx0, i) >> vrc (succ rx1, i) 
                 | i <- [succ ry0 .. ry1]
                 ]
  setSurface (SurfColor 1 1 1)
  unsafeRenderPrimitive LineLoop $ mapM_ vrc [ r0, (succ rx1, ry0)
                                             , (succ rx1, succ ry1)
                                             , (rx0, succ ry1)
                                             ]
  where
    (r0@(rx0,ry0),(rx1,ry1)) = roomRange
    vrc = vertex . glP3 . point23 (-0.1) . fst . roomCorners

drawEdgeValues :: EdgeMap Value -> IO ()
drawEdgeValues ev = mapM_ draw (assocs ev) where
  draw (e,v) = do
    setSurface (vc v)
    normal (Normal3 0 0 (1 :: Float))
    unsafeRenderPrimitive Triangles $ do
      vp $ (rc .+^ wallDir d) .+^ wallDir (succ' d)
      vp $ (rc .+^ wallDir d) .-^ wallDir (succ' d)
      --setSurface (SurfColor 0 0 0)
      vp rc
    where
      Edge r d = flop e
      rc = roomCenter r
      vp = vertex . glP3 . point23 (-0.5)
  evl = elems ev
  v0 = minimum evl
  v1 = maximum (1:evl)
  vs v = (v0,v1) ~@- v
  vc v = SurfColor s s (1-s) where s = vs v

drawMap :: Maze -> Room -> IO ()
drawMap m r = do
  drawGrid
  drawRoom m r
