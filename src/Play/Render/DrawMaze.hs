module Play.Render.DrawMaze (
    drawMaze
  ) where

import Graphics.UI.GLUT hiding (Vector2(..))
import Play.Space
import Maze.World
import Maze.Room
import Maze.Edge
import Maze
import Play.MazeGeo
import Play.Render.Draw

edir :: Dir -> Vector2 Double
edir = wallDir

edgeNorm :: Dir -> Vector
edgeNorm = vector23 0 . negateVector . dirVector
enorm :: Dir -> Vector
enorm = edgeNorm

floorBounds :: (Point2 Double, Point2 Double) -> [Point]
floorBounds = map (point23 0) . uncurry rectBounds

drawWall :: Room -> Dir -> TextureType -> IO ()
drawWall r d t@TextureInnerWall = do
  setSurface $ SurfTexture 2 t
  drawRect (vertRectBounds (wc .-^ wd, wc .-^ (doorWidth/2)*^wd) (0,0.5)) n
  drawRect (vertRectBounds (wc .+^ (doorWidth/2)*^wd, wc .+^ wd) (0,0.5)) n
  where
    n = enorm d
    wc = roomCenter r .+^ edir d
    wd = edir (succ' d)
drawWall r d t = do
  setSurface $ SurfTexture 5 t
  drawRect 
    (vertRectBounds 
      (roomCenter r .+^ edir d .-+^ edir (succ' d))
      (0,1))
    (enorm d)

doorWidth :: Double
doorWidth = 0.4

drawDoor :: Room -> Dir -> IO ()
drawDoor r d =
  unsafePreservingMatrix $ do
    setSurface $ SurfColor 0.3 0.3 0.4
    translate $ glV3 $ point23 0 (roomCenter r .+^ edir d) .-. origin
    renderObject Solid (Cylinder' 0.01 signHeight 15 1)

signSize, signHeight :: Double
signSize = 0.2
signHeight = 0.4

drawSign :: Room -> Dir -> TextureType -> IO ()
drawSign r d t = do
  setSurface $ SurfTexture 1 t
  drawRect 
    (vertRectBounds 
      (roomCenter r .+^ 0.99*^edir d .-+^ (signSize/2)*^edir (succ' d)) 
      (signHeight,signHeight+signSize))
    (enorm d)

drawWallDoor :: Room -> Dir -> TextureType -> IO()
drawWallDoor r d t = drawWall r d t >> drawDoor r d

drawSide :: Room -> Bool -> Dir -> Door -> IO ()
drawSide r _ d Wall = drawWall r d TextureWall
drawSide r _ d Door = drawWallDoor r d TextureInnerWall
drawSide r False d _ = drawWallDoor r d TextureInnerWall >> drawSign r d TextureSignHide
drawSide r True d DoorInc = drawWallDoor r d TextureInnerWall >> drawSign r d TextureSignOut
drawSide r True d DoorDec = drawWallDoor r d TextureInnerWall >> drawSign r d TextureSignIn


drawRoom :: Maze -> Room -> Bool -> IO ()
drawRoom m r curr = do
  mapM_ (\d -> drawSide r curr d $ mazeDoor m (Edge r d)) directions
  setSurface $ SurfTexture 4 TextureGround
  drawRect (floorBounds $ roomCorners r) (vector3 0 0 1)


drawMaze :: Maze -> Room -> IO ()
drawMaze m cr = mapM_ (\r -> drawRoom m r (cr == r)) roomList
