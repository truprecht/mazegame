module Play.Render.Display3D (
    display3D
  ) where

import Control.Monad
import Play.Space
import Maze.World
import Maze.Room
import Play.State
import Game
import Play.Render
import Play.Render.Draw
import Play.MazeGeo
import Play.Render.DrawMaze
import Play.Render.DrawReward
import qualified Graphics.UI.GLUT as GL

type ViewPos = (Point, Angle, Angle)

drawBackground :: Angle -> Double -> IO ()
drawBackground a _ = do setSurface $ SurfTexture 2 TextureBG
                        drawRect 
                          (map (point23 (-0.5)) $ rectBounds (Point2 (-4 - 8 * o) (-1)) (Point2 (12-8*o) 3))
                          (vector3 0 0 1)
  where
    o = snd (properFraction $ 0.3238 + circle a :: (Int, Double))

viewHeight :: Double
viewHeight = 0.6

p23h :: Point2 Double -> Point3 Double
p23h = point23 viewHeight

standPoint :: Double
standPoint = 0.9

viewAngle :: Angle
viewAngle = ofRadians (-0.05 :: Double)

viewPosEnter :: (Room, Dir) -> Float -> ViewPos
viewPosEnter (r,d) t =
  (p23h $ roomCenter r .-^ ((2-standPoint,standPoint) -@~ (t*(2-t))) *^ wallDir d, direction d, viewAngle)

standPos :: (Room, Dir) -> Point2 Double
standPos (r,d) = roomCenter r .-^ standPoint *^ wallDir d

standDir :: (Room, Dir) -> Angle
standDir (_,d) = direction d

viewPosStand :: (Room, Dir) -> ViewPos
viewPosStand rd = (p23h $ standPos rd, standDir rd, viewAngle)

viewPosTurn :: (Room, Dir) -> Dir -> Float -> ViewPos
viewPosTurn (r,d1) d2 t = (p23h $ roomCenter r .+^ standPoint *^ mp, md, viewAngle) where
  a = (0, pi/2) -@~ t
  ed = negateVector $ wallDir d1
  ed' = wallDir d2
  mp = (1 - sin a) *^ ed ^+^ (1 - cos a) *^ ed'
  md = (direction d1, direction d2) -@~ t

viewPosWarp :: (Room, Dir) -> (Room, Dir) -> Float -> ViewPos
viewPosWarp rd1 rd2 t = (point23 h p, d, va) where
  p = (standPos rd1, standPos rd2) -@~ t
  d = (standDir rd1, standDir rd2) -@~ t
  h = viewHeight + 10 * realToFrac tt2
  va = (viewAngle, ofRadians (- (pi :: Double)/4)) -@~ (4 * tt2)
  tt2 = t - t*t

drawTimeout :: Float -> IO ()
drawTimeout _
  = withViewOrtho 
  $ \_ -> do setSurface (SurfColor 1 0 0)
             drawRect (map (point23 0.4) $ rectBounds (Point2 (-1) (-0.5)) (Point2 1 0.5)) (vector3 0 0 1)
             setSurface (SurfColor 0 0 0)
             drawText (Point3 0 0 0.5) ["TIMEOUT"]

drawCueBox :: Bool -> IO ()
drawCueBox c
  = withViewOrtho 
  $ \_ -> do let color = if c
                           then SurfColor 0.5 1 0.5 
                           else SurfColor 0.5 0 0
                 coords = if c 
                            then [Point2 x0 y1, Point2 x0 y2, Point2 x1 y0, Point2 x2 y0]
                            else [Point2 x1 y2, Point2 x2 y1, Point2 x1 y1, Point2 x2 y2]
             setSurface color
             -- drawRect (map (point23 0.4) $ rectBounds (Point2 x1 y1) (Point2 x2 y2)) (vector3 0 0 1)
             GL.unsafeRenderPrimitive GL.Lines $ mapM_ (GL.vertex . glP3 . point23 0.4) coords
    where 
      (x0,y0) = (0 :: Double, -0.15 :: Double)
      s = 0.015
      (x1,x2,y1,y2) = (x0-s,x0+s,y0-s,y0+s)

display3D :: State -> IO ()
display3D state@State {
    stateGame = Game {
      gameMaze = m,
      gamePos = p@(r,_),
      gameReward = rr
    },
    stateStance = s,
    stateTime = st0
  } = do t <- time
         let st = (st0, stateNextTime state) ~@- t
             (xy,a,va) = case s of
                              StanceEnter -> viewPosEnter p st
                              StanceTurn dt -> viewPosTurn p dt st
                              StanceWarp p2 -> viewPosWarp p p2 st
                              StanceFree p' a' -> (p23h p', a', 0)
                              _ -> viewPosStand p
         withViewOrtho $ drawBackground a
         viewPoint xy a va
         drawMaze m r
         mapM_ (\rr' -> drawRoomReward m rr' (if rr' == r then if s == StanceEnter then st else 1 else 0)) roomList
         maybe (return ()) (drawReward m r (if s == StanceEnter then st else 1)) rr

         when (s == StanceTimeout) $ drawTimeout st
         drawCueBox (s == StanceStand)
