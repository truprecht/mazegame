module Play.Render.Display (
    DisMode(..),
    initDisplay,
    setDisplay,
    getNextDisplay,
    nextDisplay,
    displayBzzt
  ) where

import Data.GlobalIORef
import Graphics.UI.GLUT
import Play.State
import Play.Render
import Play.Render.Draw
import Play.Render.Display3D
import Play.Render.Display2D
import Play.Break

data DisMode = Display2D | Display3D deriving (Eq, Ord, Show)

data DisData = DisData {
    disState :: State,
    disMode :: DisMode,
    disNext :: IO (),
    disBzzt :: Bool
  }

disData :: IORef (Maybe DisData)
disData = globalIORef Nothing

display :: DisData -> IO ()
display d@DisData { disBzzt = True } = do
  drawBzzt
  writeIORef disData (Just d{ disBzzt = False })
display DisData { disState = State{ stateStance = StanceBlank } } = return ()
display DisData { disState = State{ stateStance = StanceBreak br, stateGame = g } } = drawBreak (breakMode br g)
display DisData { disState = state, disMode = Display2D } = display2D state
display DisData { disState = state, disMode = Display3D } = display3D state

idle :: IO ()
idle = readIORef disData >>= maybe (return ()) idl where
  idl DisData{ disState = state
             , disNext = next
             } = do t <- time
                    if t >= stateNextTime state
                      then do idleCallback $= Nothing
                              next
                      else postRedisplay Nothing

setDisplay :: DisMode -> State -> IO () -> IO ()
setDisplay mode state next 
  = do writeIORef disData $ Just DisData{ disMode = mode
                                        , disState = state
                                        , disNext = next
                                        , disBzzt = False
                                        }
       if stateNextTime state > stateTime state
          then idleCallback $= Just idle
          else (idleCallback $= Nothing) >> postRedisplay Nothing

getNextDisplay :: IO (Maybe (IO ()))
getNextDisplay = fmap disNext <$> readIORef disData

nextDisplay :: IO Bool
nextDisplay = readIORef disData >>= maybe (return False) next
  where
    next (DisData { disNext = n }) = n >> return True

initDisplay :: IO ()
initDisplay 
  = initDraw 
  >> (displayCallback $= (readIORef disData >>= maybe (return ()) display >> render))

displayBzzt :: IO ()
displayBzzt = modifyIORef disData (fmap (\d -> d{ disBzzt = True }))
