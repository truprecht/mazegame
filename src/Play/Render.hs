module Play.Render (
    initRender,
    openRender,
    viewPoint,
    viewOrtho,
    withStateVal,
    withMatrixMode,
    withViewOrtho,
    orthoRez,
    withDoubleSided,
    render,
    time,
    timer
  ) where

import Control.Monad
import Data.GlobalIORef
import Graphics.UI.GLUT hiding (Angle)
import Play.Space
import Maze.World

-- constants
defaultViewAngle :: GLdouble
defaultViewAngle = 100.0
defaultWinSize :: Size
defaultWinSize = Size 640 480
defaultAspect :: Double
defaultAspect = 640 / 480
defaultOrez :: GLsizei
defaultOrez = 0

--------------------------------------------------------

aspect :: IORef Double
aspect = globalIORef defaultAspect

winsize :: IORef Size
winsize = globalIORef defaultWinSize

orez :: IORef GLsizei
orez = globalIORef defaultOrez

--------------------------------------------------------

withStateVal :: StateVar a -> a -> IO () -> IO ()
withStateVal sv v f = do
  v' <- get sv
  sv $= v
  f
  sv $= v'


initRender :: IO (String, [String])
initRender = do
  initialWindowSize $= defaultWinSize
  initialWindowPosition $= Position 0 0
  getArgsAndInitialize


openRender :: Maybe Bool -> IO () -> IO ()
openRender fs run = do initialDisplayMode $= [RGBAMode, WithDepthBuffer, DoubleBuffered]
                       _ <- case fs of
                                 Nothing -> createWindow "mazegame"
                                 Just False -> do win <- createWindow "mazegame"
                                                  fullScreen
                                                  return win
                                 Just True -> do gameModeCapabilities $= [Where' GameModeWidth IsEqualTo 1024, Where' GameModeHeight IsEqualTo 768, Where' GameModeRefreshRate IsEqualTo 60]
                                                 (win, y) <- enterGameMode
                                                 unless y $ fail "enterGameMode failed"
                                                 return win
                       clear [ColorBuffer]
                       depthFunc $= Just Less
                       cullFace $= Just Back
                       shadeModel $= Flat

                       ambient (Light 0) $= Color4 0.3 0.3 0.4 1
                       diffuse (Light 0) $= Color4 0.6 0.6 0.5 1
                       ambient (Light 1) $= Color4 0.0 0.0 0.0 1
                       diffuse (Light 1) $= Color4 0.3 0.4 0.5 1
                       lightModelAmbient $= Color4 0.0 0.0 0.0 1
                       lightModelLocalViewer $= Enabled
                       light (Light 0) $= Enabled
                       light (Light 1) $= Enabled

                       reshapeCallback $= Just reshape
                       matrixMode $= Modelview 0

                       actionOnWindowClose $= MainLoopReturns
                       run
                       mainLoop
                       when (fs == Just True) leaveGameMode

reshape :: Size -> IO ()
reshape size@(Size w h)
  = do viewport $= (Position 0 0, size)
       writeIORef winsize size
       writeIORef aspect (fromIntegral w / fromIntegral h)

viewPoint :: Point -> Angle -> Angle -> IO ()
viewPoint p a va = do
  matrixMode $= Projection
  loadIdentity
  r <- readIORef aspect
  perspective defaultViewAngle r 0.1 10.0

  matrixMode $= Modelview 0
  loadIdentity
  -- Manually perform the y<->z transform because some GL implementations seem to ignore the up_z component
  lookAt (pv ((p .-^ 0 *^ ahead) .+^ vector3 0 0 0)) (pv $ p .+^ ahead) (Vector3 0 1 0)
  mt <- newMatrix RowMajor [1,0,0,0, 0,0,1,0, 0,-1,0,0, 0,0,0,1] :: IO (GLmatrix GLfloat)
  multMatrix mt
  --lookAt (Vertex3 5 5 5) (Vertex3 5 5 0) (Vector3 0 1 0)
  lighting $= Enabled
  position (Light 0) $= Vertex4 (-2) (-1) 2 0
  position (Light 1) $= Vertex4 2 0.5 3 0
  where 
    pv (Point3 x y h) = Vertex3 x h (-y)
    ahead = vector23 (sin (radians va)) (cos (radians va) *^ angle a)


viewOrtho :: IO Double
viewOrtho = do
  matrixMode $= Projection
  loadIdentity
  a <- readIORef aspect
  Size w h <- readIORef winsize
  rez <- if a > 1
    then do ortho (-a) a (-1) 1 (-1) 1
            return h
    else do ortho (-1) 1 (-1/a) (1/a) (-1) 1
            return w
  writeIORef orez rez
  --ortho (-1) 1 (-1) 1 (-1) 1
  lighting $= Disabled
  
  matrixMode $= Modelview 0
  loadIdentity

  return a

orthoRez :: IO Double
orthoRez = fromIntegral <$> readIORef orez

withMatrixMode :: MatrixMode -> IO () -> IO ()
withMatrixMode mode f = do
  withStateVal matrixMode mode $
    unsafePreservingMatrix $
      loadIdentity >> f

withViewOrtho :: (Double -> IO ()) -> IO ()
withViewOrtho f =
  withStateVal depthFunc Nothing $ 
  withStateVal lighting Disabled $
  withMatrixMode Projection $ do
    viewOrtho >>= f
    matrixMode $= Projection

withDoubleSided :: IO () -> IO ()
withDoubleSided f =
  withStateVal cullFace Nothing $
    withStateVal lightModelTwoSide Enabled f

render :: IO ()
render = do
  swapBuffers
  flush
  clear [ColorBuffer, DepthBuffer]

time :: IO Time
time = ((/1000) . fromIntegral) <$> get elapsedTime

timer :: Time -> IO () -> IO Time
timer t f = do
  t0 <- time
  let dt = t - t0
  addTimerCallback (ceiling (1000*dt)) f
  return dt
