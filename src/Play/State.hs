module Play.State (
    Stance(..),
    State(..),
    statePos, stateRoom, stateDir,
    stanceDuration
  ) where

import Data.Rand
import Play.Space
import Maze.World
import Maze.Room
import Maze.Edge
import Game
import Game.Param

data Stance = 
    StanceEnter 
  | StanceStand
  | StanceTurn Dir
  | StanceWarp (Room, Dir)
  | StanceTimeout
  | StanceBlank
  | StanceBreak (Maybe Int)
  | StanceFree (Point2 Double) Angle
  deriving (Eq, Show)

data State = State {
    stateGame :: !Game,
    stateStance :: !Stance,
    stateTime :: !Time,
    stateNextTime :: !Time,
    stateEdgeValues :: Maybe (EdgeMap Value)
  }

statePos :: State -> (Room, Dir)
statePos = gamePos . stateGame

stateRoom :: State -> Room
stateRoom = fst . statePos
stateDir :: State -> Dir
stateDir = snd . statePos

stanceDuration :: Stance -> RandM Time
stanceDuration StanceEnter = return enterTime
stanceDuration StanceStand = return standTimeout
stanceDuration (StanceTurn _) = rand (turnTime,turnTime+jitterTime)
stanceDuration (StanceWarp _) = rand (warpTime,warpTime+jitterTime)
stanceDuration StanceTimeout = rand (1::Time,1+jitterTime)
stanceDuration (StanceBreak (Just 0)) = rand (1 :: Time,1+scanTR)
stanceDuration _ = return 0
