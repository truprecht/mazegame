{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, GeneralizedNewtypeDeriving, FlexibleContexts #-}

module Play.Space (
    module AF,
    Point, Vector,
    Angle,
    ofRadians, radians, degrees, circle,
    angle, vectorAngle,
    point2app, point3app,
    vector2app, vector3app,
    point23,
    vector23,
    glP2, glP3,
    glV2, glV3,
    (-@~), (~@-),
    midpoint,
    (.-+^)
  ) where

import qualified Graphics.UI.GLUT as GL
import FRP.Yampa.Geometry as AF

type Point = Point3 Double
type Vector = Vector3 Double

newtype Angle = Angle { unAngle :: Float } deriving (Show, Num)

instance Eq Angle where
  a == b = unAngle (normAngle a) == unAngle (normAngle b)
  
twopi :: Float
twopi = 2*pi

normAngle :: Angle -> Angle
normAngle = vectorAngle . angle

angle :: Angle -> Vector2 Double
angle (Angle a) = let d = realToFrac a
                  in vector2 (sin d) (cos d)

ofRadians :: (RealFloat a) => a -> Angle
ofRadians = Angle . realToFrac

radians :: (RealFloat a) => Angle -> a
radians (Angle a) = realToFrac $ a

degrees :: (RealFloat a) => Angle -> a
degrees (Angle a) = realToFrac $ 180*a/pi

circle :: (RealFloat a) => Angle -> a
circle (Angle a) = let (_, f) = properFraction (a / twopi) :: (Int, Float)
                   in realToFrac $ if f < 0 then f+1 else f

vectorAngle :: Vector2 Double -> Angle
vectorAngle = Angle . realToFrac . vector2app atan2

point2app :: RealFloat a => (a -> a -> b) -> Point2 a -> b
point2app f (Point2 x y) = f x y

point3app :: RealFloat a => (a -> a -> a -> b) -> Point3 a -> b
point3app f (Point3 x y z) = f x y z

vector2app :: RealFloat a => (a -> a -> b) -> Vector2 a -> b
vector2app f p = f (vector2X p) (vector2Y p)

vector3app :: RealFloat a => (a -> a -> a -> b) -> Vector3 a -> b
vector3app f p = f (vector3X p) (vector3Y p) (vector3Z p)

point23 :: RealFloat a => a -> Point2 a -> Point3 a
point23 z (Point2 x y) = Point3 x y z

vector23 :: RealFloat a => a -> Vector2 a -> Vector3 a
vector23 z v = vector3 (vector2X v) (vector2Y v) z

glP2 :: RealFloat a => AF.Point2 a -> GL.Vertex2 a
glP2 = point2app GL.Vertex2

glP3 :: RealFloat a => AF.Point3 a -> GL.Vertex3 a
glP3 = point3app GL.Vertex3

glV2 :: RealFloat a => AF.Vector2 a -> GL.Vector2 a
glV2 = vector2app GL.Vector2

glV3 :: RealFloat a => AF.Vector3 a -> GL.Vector3 a
glV3 = vector3app GL.Vector3

instance (RealFloat a, Show a) => Show (Point3 a) where
  show (Point3 x y z) = '<' : show x ++ ',' : show y ++ ',' : show z ++ ">"


infix 5 -@~, ~@-
class Interpolable a b where
  (-@~) :: (a,a) -> b -> a
  (~@-) :: (a,a) -> a -> b

instance Interpolable Double Double where
  (x0,x1) -@~ t = x0+t*(x1-x0)
  (x0,x1) ~@- x = (x-x0)/(x1-x0)

instance Interpolable Float Float where
  (x0,x1) -@~ t = x0+t*(x1-x0)
  (x0,x1) ~@- x = (x-x0)/(x1-x0)

instance Interpolable Double Float where
  (x0,x1) -@~ t = x0 + realToFrac t * (x1-x0)
  (x0,x1) ~@- x = realToFrac $ (x-x0)/(x1-x0)

instance Interpolable (Point2 Double) Double where
  (x0,x1) -@~ t = x0.+^t*^(x1.-.x0)
  (x0,x1) ~@- x = (x.-.x0) `dot` normalize (x1.-.x0)

instance Interpolable (Point2 Double) Float where
  (x0,x1) -@~ t = x0 .+^ realToFrac t *^ (x1.-.x0)
  (x0,x1) ~@- x = realToFrac $ (x.-.x0) `dot` normalize (x1.-.x0)

normAngleR :: (Angle, Angle) -> (Float, Float)
normAngleR (aa,ba) = r where
  Angle a = normAngle aa
  Angle b = normAngle ba
  r 
    | b-a > pi = (a,b-twopi)
    | b-a < -pi = (a-twopi,b)
    | otherwise = (a,b)

instance Interpolable Angle Float where
  ar -@~ t = Angle $ normAngleR ar -@~ t
  ar ~@- aa = normAngleR ar ~@- a where Angle a = normAngle aa

instance Interpolable (Point2 Double) (Point2 Double) where
  (Point2 x0 y0, Point2 x1 y1) -@~ (Point2 x y) = Point2 ((x0,x1) -@~ x) ((y0,y1) -@~ y)
  (Point2 x0 y0, Point2 x1 y1) ~@- (Point2 x y) = Point2 ((x0,x1) ~@- x) ((y0,y1) ~@- y)

midpoint :: Interpolable a Double => a -> a -> a
midpoint x y = (x, y) -@~ (0.5 :: Double)

infix 5 .-+^
(.-+^) :: AffineSpace t v a => t -> v -> (t, t)
(.-+^) p v = (p .-^ v, p .+^ v)
