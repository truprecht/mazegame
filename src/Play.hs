import Control.Monad
import Data.List
import Data.List.Extra (ppNestedList)
import qualified System.Exit
import qualified System.Random as Random
import qualified System.Console.GetOpt as GetOpt
import qualified System.Time as Time
import qualified System.Posix.Files as F
import qualified System.IO.Strict as SIO
import Data.Rand
import Game
import Game.History
import Play.Play
import Play.Render (initRender)
import Play.Render.Display
import Maze.MazeGraph
import Game.Param
import Data.Maybe (fromMaybe)

data Mode = ModePlay PlayMode deriving (Show)

data Options = Options
  { optionVerbose :: Bool
  , optionSimSeed :: Maybe Random.StdGen
  , optionMazeSeed :: Maybe Random.StdGen
  , optionFullScreen :: Bool
  , optionDisplay :: DisMode
  , optionMode :: Mode
  , optionSteps :: Int
  , optionHistory :: Maybe FilePath
  , optionGameFile :: Maybe FilePath
  , optionDumpFile :: Maybe FilePath
  }

defaultOptions :: Options
defaultOptions = Options
  { optionVerbose = True
  , optionSimSeed = Nothing
  , optionMazeSeed = Nothing
  , optionFullScreen = False
  , optionDisplay = Display3D
  , optionMode = ModePlay PlayTest
  , optionSteps = 500
  , optionHistory = Nothing
  , optionGameFile = Nothing
  , optionDumpFile = Nothing
}

options :: [GetOpt.OptDescr (Options -> Options)]
options =
  [ GetOpt.Option "q" ["quiet"] (GetOpt.NoArg (\ x -> x{ optionVerbose = False })) ("don't produce extra output")
  , GetOpt.Option "" ["generation-seed"] (GetOpt.ReqArg (\ str x -> x{ optionMazeSeed = Just $ read str }) "STR") ("use random seed of STR for maze generation")
  , GetOpt.Option "" ["simulation-seed"] (GetOpt.ReqArg (\ str x -> x{ optionSimSeed = Just $ read str }) "STR") ("use random seed of STR for the simulation behavior")
  , GetOpt.Option "g" ["read-game"] (GetOpt.ReqArg (\ str x -> x{ optionGameFile = Just str }) "STR") ("use game definition from FILE")
  , GetOpt.Option "d" ["dump-game"] (GetOpt.ReqArg (\ str x -> x{ optionDumpFile = Just str }) "STR") ("dump used game definition to FILE")
  , GetOpt.Option "F" ["full-screen"] (GetOpt.NoArg (\ x -> x{ optionFullScreen = True })) ""
  , GetOpt.Option "a" ["append"] (GetOpt.ReqArg (\ str x -> x{ optionHistory = Just str }) "FILE") ("append final play history to FILE")
  , GetOpt.Option "n" ["steps"] (GetOpt.ReqArg (\ str x -> x{ optionSteps = read str }) "NUM") ("play at most NUM steps, default: 500")
  , GetOpt.Option "2" ["display-2D"] (GetOpt.NoArg (\ x -> x{ optionDisplay = Display2D })) ("display mode 2D" ++ if optionDisplay defaultOptions == Display2D then " [default]" else "")
  , GetOpt.Option "3" ["display-3D"] (GetOpt.NoArg (\ x -> x{ optionDisplay = Display3D })) ("display mode 3D" ++ if optionDisplay defaultOptions == Display3D then " [default]" else "")
  , GetOpt.Option "y" [] (GetOpt.NoArg (\ x -> x{ optionMode = ModePlay PlayReal })) ("non-test mode")
  , GetOpt.Option "Y" [] (GetOpt.NoArg (\ x -> x{ optionMode = ModePlay PlayScan })) ("scanner mode")
  ]

main :: IO ()
main = do
  (prog, args) <- initRender
  let defopts = defaultOptions
  opt <- case GetOpt.getOpt GetOpt.Permute options args of
              (o, [], []) -> return $ foldl' (flip ($)) defopts o
              (_, _, err) -> do mapM_ putStrLn err
                                putStrLn $ GetOpt.usageInfo ("Usage: " ++ prog ++ " [OPTIONS]") options
                                System.Exit.exitFailure
  
  simseed <- fromMaybe Random.newStdGen (return <$> optionSimSeed opt)
  genseed <- fromMaybe Random.newStdGen (return <$> optionMazeSeed opt)
  
  now <- Time.getClockTime
  when (optionVerbose opt) 
    $ print ( optionMode opt
            , optionDisplay opt
            , now
            , simseed
            , genseed
            )

  game <- maybe (return $ runRand genseed $ newGame (optionSteps opt)) ((read <$>) . readFile) (optionGameFile opt)
  maybe (return ()) (`write` show game) (optionDumpFile opt)
  
  hist <- case optionMode opt of
               (ModePlay mode) -> play PlayOptions{ playMode = mode
                                                  , playFS = optionFullScreen opt
                                                  , playDisplay = optionDisplay opt
                                                  , playEvents = Nothing
                                                  , playVerbose = optionVerbose opt
                                                  } game

  case optionHistory opt of
       Nothing -> return ()
       Just historyFile -> do fe <- F.fileExist historyFile
                              previousHistory <- if fe
                                                    then read <$> SIO.readFile historyFile
                                                    else return []
                              writeFile historyFile $ ppNestedList (hist : previousHistory)

  when (optionVerbose opt) $ do
    let r = historyRewards hist
        p = r / fromIntegral (length $ filter isHistory hist)
        h = horizonValue (gameMaze game)
    putStrLn ("\n" ++ show ((round $ rewardMult * r) :: Int) ++ ", " ++ show (100 * (p/h - 1)) ++ "%")
    case optionMode opt of
         ModePlay PlayScan -> putStrLn ("$" ++ show (rewardMult*r*0.04))
         _ -> return ()

  

write :: FilePath -> String -> IO ()
write "-" = putStrLn
write fnm = writeFile fnm
