# Changelog

## init
* initial release by Dylan Simon
* cannot be compiled due to base dependency issues

## 04/05/2017
* removed dependency to load textures as it is not available anymore
* added some explicit ghc extensions
* some minor adaptions to newer package versions
* can be compiled and run in graphical mode

## 07/06/2017
* removed matlab libraries and used the model definitions provided in haskell to maintain functionality
* removed option to dump history in matlab file format
* replaced outdated libraries with new versions, adapted some function calls
* removed record accessor pattern and class that caused some errors
* removed other template-haskell calls
* removed wrapper type for derivations as it was not used
* lots of refactorization, structured source files in hirachical folder structure
* works in both, graphical and simulation mode, now
- uploaded first version of the virtual machine

## 21/06/2017
* replaced polymorph model type that caused some errors with a sum type
* similar models now share the same functions and types
* removed redundant functions in Data.Util
* removed remaining template haskell
* updated dependencies to work with ghc 8

## 26/06/2017
* separated rng seeds to differentiate random maze generation / warps / doorflips, random simulation behavior, and other non-important calls
* added option to read a game file and skip random generation
* adapted game procedure to two steps: generate / read a game definition (including maze, warps, doorflips) and execution rather than generating random events on the fly

## 27/06/2017
* cli parameter definitions for simulations now may contain multiple parameters
* updated readme
- uploaded second version of the virtual machine

## 04/07/2017
* removed some distracting warnings during game
* took care of ghc's warnings
* added option to dump game file for re-running a game
* added changelog

## 12/07/2017
* added script to visualize maze structure from string

## 18/07/2017
* implemented likelihood and its gradient (wrt. the model parameters) for a history and a given model
* added option and functionality to fit a model using a history file
* cli is a hazzle

## 19/07/2017
* separated simulation, game and fitting into own executables
* updated readme

## 25/07/2017
* added script to convert history files
* fitting uses a list of histories

## 01/08/2017
* added script to generate game files
* fixed gradient descent
* added BIC and rho^2 measure to fitting log
* moved io libraries to game executable, simulation and fitting do not require opengl etc. anymore

## 07/08/2017
* added script to fit model parameters to each subject in folder
* updated readme

# 21/08/2017
* added backwards conversion for history files
* removed some redundant functions from ModelQLA

# 28/08/2017
* updated readme
* splitted conversion script
* updated fitting script

# 05/09/2017
* added forward planing model (w/o fixed depth), added to fitting
* fitting script prints a *beautiful* table

# 30/01/2018
* gradient updates for certain model / parameter configurations are disables
    * q0, alpha, lambda, depth for the forward planing model
    * door decay, depth for temporal difference model
* default parameters are updated to use the actual published parameters
  for the forward planing model and temporal difference model
* sample range for random chosen parameters are not considered the domain anymore