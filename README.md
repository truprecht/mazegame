# General
This project implements a game in Haskell that was used to identify and characterize human behavior in a reward-based decision process.
Along with the game itself, there is a simulation that uses different models to reproduce a game.

The initial version of this project was implemented by Dylan Simon for the paper "Neural Correlates of Forward Planning in a Spatial Decision Task in Humans" (http://www.jneurosci.org/content/jneuro/31/14/5526.full.pdf). 

# Usage
## Compiling
The compilation is handled via cabal-install.
So you can just run `cabal install` in the project's root folder to install it and all of its dependencies.
After installing, cabal will provide three executables, `maze-game`, `maze-sim` and `maze-fit`.

## Execution
The execution is handled by cabal as well. Just run `cabal exec maze-{game|sim|fit}`.
Each of those executables handles one particular action:
* `maze-game` launches the graphical maze game,
* `maze-sim` is used for the simulation using a particular model and
* `maze-fit` fits a model to a behavioral history file of a subject in a maze

Command line arguments for the application are separated by `--` from cabal. E.g. if you want to use the option `-3` in a call of `maze-game`, you have to launch `cabal exec maze-game -- -3`.

### maze-sim
`cabal exec maze-sim -- <MODEL>` will start the executable for simulation mode.
It requires a behavior model as argument.

#### Models
Available models are
* ModelDumb - decides each step using a equally distributed random variable -,
* ModelFull - a naive implementation of the fixed depth forward planing model -,
* ModelQLA - a more efficient iterative implementation of the fixed depth forward planing model -,
* ModelFP - a more efficient iterative implementation of the forward planing model -,
* ModelRESucc - the room edge successor model -,
* ModelRASR - the room action successor room model - and
* ModelTD - the temporal difference model.

The parameters of the corresponding model can be altered by using `-p "<PARAMETER>=<VALUE> <PARAMETER>=<VALUE> ..."`, where a parameter name is one of
* discount (default: 0.9),
* door-decay (default: 0.083),
* door-rate (default: 1),
* value-rate (default: 0.2),
* value-init (default: 0.125),
* lambda (default: 0),
* temp (default: 4), or
* depth (default: 16),

and each value is a floating point value. So, e.g. for setting modifying the depth and the lambda parameter of a model during simulation we can launch `cabal exec maze-sim -- ModelQLA -p "depth=10 lambda=0.9"`

#### Randomness
Although the behavior of the simulation and the initial generation of the maze are random, both parts can be seeded separately.
The options `--generation-seed=<SOME STRING>` and `--simulation-seed=<SOME STRING>` initialize the respective random generator (note, that `simulation-seed` does not affect the simulation in visual mode).
Plus, the random generation of the maze can be skipped completely by using a pre-defined initial game file and passing it using `-f <FILE>` to the application.
This file contains the initial maze structure and the initial position, all occurring warps with their corresponding destinations (including the room and the direction), and all door flips per step (again, consisting of a room and a direction).
There is an example file `example.maze` containing such a structure.
The other way around, such a game file can be saved by using `-d <FILE>`.
This will dump the used maze definition etc. to a specified file to repeat an experiment.

These files may also be generated using the script `scripts/init-game.py`.
It will use two preset definitions for the initial door configuration, door flips and jump destinations and thus it expects exactly one argument which is either `A` or `B`.
For example you can use `python3 scripts/init-game.py A > example-a.maze` to save a file 'example-trial-a' with initial setting for a maze of version A.

#### History
The history, that contains the maze configuration at each step and the player's movement and rewards, is printed per default.
The contents of a history is a table whose rows correspond to each applied step with the following columns:
* the start timestamp of the step,
* the subject's choice,
* the subject's reaction timestamp,
* the current step,
* the current maze structure,
* the subject's current position (two columns: room and direction) and
* gathered rewards for the current step.

The structure of the maze is dumped in a string that encodes the direction of each door, and the position of the rewards.
As the string is quite hard to read, there is a python script, `scripts/draw-maze.py`, to visualize the structure of such a maze dump.
A call of `python3 draw-maze.py -m <MAZE DUMP>` will draw the maze structure with arrows to indicate the direction of open doors.

These history files can be converted into a table using the script `scripts/history-to-table.py`.
E.g. `python3 scripts/history-to-table.py example-A.history > example-A.table` will convert the history contianed in the file 'example-A.history' into a table and store it ni the file 'example-A.table'.
This will only work with histories of simulations or trials using the supplied jump patterns, for now.

#### Misc
Multiple applications and scripts can be piped.
For example, a call of `python3 scripts/init-game.py A | cabal exec maze-sim -- ModelQLA -f - -p "temp=10 lambda=0.1" | python3 scripts/history-to-table.py` will
* generate an initial maze configuration,
* use that configuration to simulate the forward planing model with modified parameters, and
* convert the resulting steps into table format and print it.
But keep in mind that intermediate results will not be saved.

### maze-game
Per default, the application will launch in 3d visual mode. I.e. a window will pop up where you have to navigate through the maze using the arrow keys.
Although you may run it explicitly using `-3` for 3d mode or `-2` for 2d game mode.
Beyond the arrow keys, there are key bindings for the following actions in the maze:
* q - to quit
* ~~n - to force a random warp~~
* f - to enter a free motion mode, where you can do nothing but move around the maze
* s - to toggle simulation mode, where all actions are handled by a model (default: DumbModel)
* m - to change the model for simulation mode
* p - to change the model parameters for simulation mode
* ~~v - to calculate model values for all positions~~
* ` - tbd
* c - tbd

You may dump the subject behavior as a history file using the option `-w <FILENAME>`.
The content of that file is described in section maze-sim/History.

Plus, the generation of the maze may be manipulated as described in section maze-sim/Randomness.

### maze-fit
A call of `cabal exec maze-fit -- <HISTORY> <MODEL>` launches a fitting algorithm to find the best possible model parameters according to the actions in the history.
In detail, this will compute the likelihood of all the actions listed in the history using the model's probability distribution of those actions.
At the same time, an automated differentiation library finds the gradient of this likelihood with respect to the model's paramters.
In the following, the parameters are updated according to the gradient.
Iterating this step, the likelihood should increase with the updated parameters.

The script `scripts/fit-singles.sh` is a wrapper around `maze-fit`.
It fits a model for each subject's log file in a folder and prints the median parameter values and likelihood measures of each last fitting step.
`bash scripts/fit-singles.sh data` expects the two subfolders `Ver_A` and `Ver_B` in the folder `data` to seperate subjects of the two studies.