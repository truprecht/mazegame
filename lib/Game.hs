module Game (
    Game(..),
    gameRoom, gameDir,
    newGame,
    stepGame,
    GameStep(..)
  ) where

import Data.Rand
import Maze.World
import Maze.Room
import Maze.Edge
import Maze
import Maze.FullFlip
import Maze.Reward
import Game.Param
import Control.Monad (replicateM)

newMaze :: RandM Maze
newMaze = flipMany initFullMaze >>= placeRewards

place :: Maze -> RandM (Room, Dir)
place m = do
  sr <- rand roomList
  let dir' = filter (not . doorExit . mazeDoor m . Edge sr . flop) directions
  sd <- rand (if null dir' then directions else dir')
  return (sr, sd)

data GameStep = StepStart | StepTurn Turn | StepDir Dir | StepWarp | StepBad | StepTimeout | StepStop deriving (Eq, Show, Read)

data Game = Game {
    gameStep :: GameStep,
    gameMaze :: Maze,
    gamePos :: (Room, Dir),
    gameReward :: Maybe Reward,
    gameStopSteps, gameTotalSteps :: Int,
    warps :: [Maybe (Room, Dir)],
    doorFlips :: [[Edge]]
  } deriving (Show, Read)

gameRoom :: Game -> Room
gameRoom = fst . gamePos
gameDir :: Game -> Dir
gameDir = snd . gamePos

gameReplace :: Game -> Maze -> RandM Game
gameReplace g m = do pos <- place m
                     return $ gameMove g{ gameMaze = m } pos

newGame :: Int -> RandM Game
newGame n = do m <- newMaze
               ws <- replicateM (max 0 $ n-3) $ do b <- rand warpProb
                                                   if b
                                                     then Just <$> place m
                                                     else return Nothing
               dfss <- replicateM n $ randSubset (rand (recip $ fromIntegral $ length edgeMapList :: Rational)) edgeMapList
               gameReplace Game{ gameStep = StepStart
                               , gameStopSteps = n
                               , gameTotalSteps = 0
                               , gameMaze = undefined
                               , gamePos = undefined
                               , gameReward = undefined
                               , warps = replicate (min 3 n) Nothing ++ ws
                               , doorFlips = dfss
                               } m

gameMove :: Game -> (Room, Dir) -> Game
gameMove g rd@(r,_) = g{ gamePos = rd, gameReward = mazeReward (gameMaze g) r }


stepGame :: Game -> GameStep -> Game
stepGame g@Game{ warps = Just (r,d) : ws, gameMaze = m} fallback
  | null $ filter (doorExit . mazeDoor m . Edge r) (map (d >+) turns) = stepGame g{ warps = Nothing:ws } fallback
  | otherwise = cleanup $ (gameMove g (r,d)){ gameStep = StepWarp }
stepGame g@Game{} StepStart 
  | gameStep g == StepStart = cleanup g{ gameStep = StepStart }
  | otherwise = stepGame g StepBad
stepGame g@Game{ gameMaze = m, gamePos = (r,d) } (StepDir d')
  | d' == flop d = stepGame g StepBad
  | not $ doorExit $ mazeDoor m $ Edge r d' = stepGame g StepBad
  | otherwise = cleanup $ (gameMove g (r |> d',d')){ gameStep = StepDir d' }
stepGame g@Game{ gamePos = (_, d) } (StepTurn dt) = stepGame g (StepDir $ d >+ dt)
stepGame g StepTimeout = cleanup $ g{ gameStep = StepTimeout }
stepGame g s = g{ gameStep = s }


cleanup :: Game -> Game
cleanup g@Game{ gameStopSteps = 1, warps = [_], doorFlips = [_] } = g{ gameStep = StepStop }
cleanup g@Game{ gameStopSteps = n, warps = _:ws, doorFlips = dfs:dfss, gameTotalSteps = m, gameMaze = maze}
  = g{ gameMaze = flipDoors maze (gamePos g) dfs, gameStopSteps = n-1, warps = ws, doorFlips = dfss, gameTotalSteps = m+1 }
cleanup _ = error "Game step with unsynchronous steps, flips and warps."
