module Maze (
    Maze,
    mazeRewards,
    initGridMaze,
    mazeDoor, mazeRoomDoors,
    mazeDoorSet, mazeDoorFlip,
    mazeReward, mazeRewardSet,
    printMaze
  ) where

import Data.Array
import Data.List
import Data.Maybe
import Maze.World
import Maze.Room
import Maze.Edge
import Game.Param (mazeSize)
import Control.Arrow (first)

xs, ys :: Int
(xs,ys) = mazeSize

data Maze = GridMaze {
  mazeDoors :: EdgeMap Door,
  mazeRewards :: RoomMap (Maybe RewardParam)
}

initGridMaze :: (Edge -> Door) -> Maze
initGridMaze doors = m where
  m = GridMaze {
    mazeDoors = listArray edgeMapRange [ doors ix | ix <- range edgeMapRange ],
    mazeRewards = listArray roomRange $ repeat Nothing
  }

mazeDoor :: Maze -> Edge -> Door
mazeDoor = getEdge . mazeDoors

mazeDoorSet :: Maze -> Edge -> Door -> Maze
mazeDoorSet m@GridMaze{ mazeDoors = edgemap } r d = m{ mazeDoors = setEdge edgemap r d }

mazeDoorFlip :: Maze -> Edge -> Maze
mazeDoorFlip m@GridMaze{ mazeDoors = edgemap } r = m{ mazeDoors = flopEdge edgemap r }

mazeRoomDoors :: Maze -> Room -> [Door]
mazeRoomDoors m r = map (mazeDoor m . Edge r) directions

mazeReward :: Maze -> Room -> Maybe RewardParam
mazeReward m r = mazeRewards m ! r

mazeRewardSet :: Maze -> Room -> Maybe RewardParam -> Maze
mazeRewardSet m@GridMaze{ mazeRewards = rr } r p = m{ mazeRewards = rr // [(r, p)] }

printMaze :: Maze -> [String]
printMaze (GridMaze { mazeDoors = doors } ) = end : vrow ys : rows (pred ys)
  where
    rows 0 = [end]
    rows y = hrow y : vrow y : rows (pred y)

    hrow y = hrowc $ map (\x -> hdoor $ doors ! Edge (x,y) North) [1..xs]
    vrow y = vrowc $ map (\x -> vdoor $ doors ! Edge (x,y) East)  [1..xs-1]
    end = hrowc $ replicate xs (hdoor Wall)
    hrowc l = [corner] ++ intersperse corner l ++ [corner]
    vrowc l = [vdoor Wall, room] ++ intersperse room l ++ [room, vdoor Wall]

    hdoor d = "-v^ " !! fromEnum d
    vdoor d = "|<> " !! fromEnum d
    corner = '+'
    room = ' '

doorChar :: Door -> Char
doorChar Wall = '0'
doorChar DoorDec = '-'
doorChar DoorInc = '+'
doorChar Door = '1'

charDoor :: Char -> Door
charDoor '0' = Wall
charDoor '-' = DoorDec
charDoor '+' = DoorInc
charDoor '1' = Door
charDoor _ = error "Maze: Trying to read undefined character for door definition."

type MazeDump = (String, [(Room,RewardParam)])

dumpMaze :: Maze -> MazeDump
dumpMaze mz = (
    map doorChar $ elems $ mazeDoors mz,
    mapMaybe rr (assocs $ mazeRewards mz)
  ) where
  rr (_,Nothing) = Nothing
  rr (r,Just w) = Just (r,w)

loadMaze :: MazeDump -> Maze
loadMaze (dl,rl) = GridMaze {
    mazeDoors = listArray edgeMapRange $ map charDoor dl,
    mazeRewards = accumArray (const Just) Nothing roomRange rl
  }

instance Show Maze where 
  show = show . dumpMaze
instance Read Maze where 
  readsPrec i = map (first loadMaze) . readsPrec i
