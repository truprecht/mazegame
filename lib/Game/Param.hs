module Game.Param where

import Maze.World

turnTime, enterTime, warpTime, standTimeout, jitterTime, scanTR :: Time
turnTime = 1.5
enterTime = 0.5
warpTime = turnTime + enterTime
standTimeout = 2
jitterTime = 0.5 
scanTR = 2

warpProb :: Rational
warpProb = recip 10
minWarpSteps :: Int
minWarpSteps = 3
breakSteps :: Int
breakSteps = 250

mazeSize :: (Int,Int)
mazeSize = (4,4)
rewardParams :: [RewardParam]
rewardParams = [0.39999999, 0.40000001, 0.59999999, 0.60000001]
rewardCount :: Int
rewardCount = length rewardParams
rewardMult :: Value
rewardMult = 5
