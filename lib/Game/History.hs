module Game.History (
    History(..),
    histDecisions,
    isHistory,
    normHistory,
    historyRewards
  ) where

import Maze.World
import Maze.Room
import Maze
import Game
import Control.Arrow (first)
import Data.Maybe (fromMaybe)

data History = History {
    histTime :: !Time,
    histChoice :: !GameStep,
    histChoiceTime :: !Time,
    histGame :: !Game
  } 
  | HistoryTime0 !Time 
  | HistoryBacktick !Time 
  | HistoryBreak !Time
  | HistoryKeyPress !Time !Char
--  deriving (Show, Read)

histDecisions :: [History] -> [(Game, Dir)]
histDecisions (History{ histGame = game } : h@History{ histChoice = StepDir direction } : hs) = (game, direction) : histDecisions (h:hs)
histDecisions (_ : hs) = histDecisions hs
histDecisions [] = []

isHistory :: History -> Bool
isHistory (History{}) = True
isHistory _ = False

newtype RewardDump = RewardDump { unRewardDump :: Reward }
instance Show RewardDump where 
  showsPrec p (RewardDump 0) = showsPrec p False
  showsPrec p (RewardDump 1) = showsPrec p True
  showsPrec p (RewardDump x) = showsPrec p x

instance Read RewardDump where
  readsPrec p = map (first RewardDump) . rr where
    rr s = case readsPrec p s of
                [] -> map (first (\ b -> if b then 1 else 0)) $ readsPrec p s
                x -> x

data HistDump = 
    Old (Time,GameStep,GameStep,Maze,Room,Dir,Maybe RewardDump) 
  | S Time GameStep Time GameStep Maze Room Dir (Maybe RewardDump)
  | T Time 
  | T0 Time 
  | B Time 
  | K Time Char
  deriving (Show, Read)

dumpHist :: History -> HistDump
dumpHist (History t1 c t2 (Game s m (r,d) rr _ _ _ _)) = S t1 c t2 s m r d (fmap RewardDump rr)
dumpHist (HistoryBacktick t) = T t
dumpHist (HistoryTime0 t) = T0 t
dumpHist (HistoryBreak t) = B t
dumpHist (HistoryKeyPress t k) = K t k

loadHist :: HistDump -> History
loadHist (S t1 c t2 s m r d rr) = History t1 c t2 (Game s m (r,d) (fmap unRewardDump rr) (-1) (-1) [] [])
loadHist (Old (t,c,s,m,r,d,rr)) = History (-1) c t (Game s m (r,d) (fmap unRewardDump rr) (-1) (-1) [] [])
loadHist (T t) = HistoryBacktick t
loadHist (T0 t) = HistoryTime0 t
loadHist (B t) = HistoryBreak t
loadHist (K t k) = HistoryKeyPress t k

instance Show History where show = show . dumpHist
instance Read History where 
  readsPrec n s@('(':_) = readsPrec n ("Old " ++ s)
  readsPrec n s = map (first loadHist) $ readsPrec n s

normHistory :: Bool -> [History] -> [History]
normHistory is = norm 0 undefined
  where
    norm _ _ [] = []
    norm t0 d (h@(History _ s _ g):l) = (if i (gameStep g) then (h':) else id) $ norm t0 d' l
      where
        i StepBad = False
        i StepTimeout = False
        --i StepStart = False
        i _ = True
        h' = h{ histChoice = ns s
              , histTime = histTime h - t0
              , histChoiceTime = histChoiceTime h - t0 
              }
        d' = snd $ gamePos g
        ns (StepTurn t) = StepDir (d >+ t)
        ns s' = s'
    norm _ d (HistoryTime0 t:l) 
      | is = HistoryTime0 0 : norm t d l
    norm t0 d (HistoryBreak t:l)
      | is = HistoryBreak (t - t0) : norm t0 d l
    norm t0 d (_:l) = norm t0 d l

historyRewards :: [History] -> Value
historyRewards = sum . map hr
  where
    hr h@History{} = fromMaybe 0 (gameReward $ histGame h)
    hr _ = 0
