module Maze.Reward (
    
    placeRewards
  ) where

import Data.Rand
import Maze.Room
import Maze
import Game.Param

allRots :: [Room] -> [[Room]]
allRots l = map (`map` l) [r0,r1,r2,r3,l0,l1,l2,l3] where
  r0 (x,y) = (  x,  y)
  l1 (x,y) = (  x,f y)
  l3 (x,y) = (f x,  y)
  r2 (x,y) = (f x,f y)
  l0 (x,y) = (  y,  x)
  r1 (x,y) = (  y,f x)
  r3 (x,y) = (f y,  x)
  l2 (x,y) = (f y,f x)
  f i = fst mazeSize + 1 - i

placements :: [[Room]]
placements = allRots [(1,1),(2,4),(4,1),(4,3)]

placeRewards :: Maze -> RandM Maze
placeRewards m = do places <- rand placements
                    return $ foldl (\m' (rr,r) -> mazeRewardSet m' r (Just rr)) m
                           $ zip rewardParams places
