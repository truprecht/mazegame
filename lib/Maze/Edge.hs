{-# LANGUAGE ExistentialQuantification, RankNTypes #-}

module Maze.Edge (
    Edge(..),
    edgeMapRange, edgeCount, edgeMapList,
    edgeFullRange, edgeFullList,
    roomEdges,
    edgeDest,
    EdgeMap,
    normEdge, mnormEdge, enormEdge,
    Edgeable(..),
    getEdge, setEdge, flopEdge,
  ) where

import Data.Array
import Maze.World
import Maze.Room

data Edge = Edge { edgeR :: Room, edgeD :: Dir } deriving (Eq, Show, Read)

instance Floppable Edge where
  flop (Edge r d) = Edge (r |> d) (flop d)

roomRangeAdj :: (Room,Room) -> Dir -> (Room,Room)
roomRangeAdj (r1,(x2,y2)) North = (r1,(x2,pred y2))
roomRangeAdj (r1,(x2,y2)) East = (r1,(pred x2,y2))
roomRangeAdj ((x1,y1),r2) South = ((x1,succ y1),r2)
roomRangeAdj ((x1,y1),r2) West = ((succ x1,y1),r2)

instance Ord Edge where
  compare (Edge r1 d1) (Edge r2 d2) = compare (d1,r1) (d2,r2)


instance Ix Edge where
  range (Edge r1 d1, Edge r2 d2) = [ Edge r d
                                   | d <- range (d1, d2)
                                   , r <- range (roomRangeAdj (r1, r2) d)
                                   ]

  inRange (Edge r1 d1, Edge r2 d2) (Edge r d) =  inRange (d1,d2) d
                                              && inRange (roomRangeAdj (r1,r2) d) r

  rangeSize (Edge r1 d1, Edge r2 d2) = sum 
                                     $ map (rangeSize . roomRangeAdj (r1,r2))
                                     $ range (d1,d2)
  
  index (Edge r1 d1, Edge r2 _) (Edge r d)
    = (if d <= d1 then 0 else rangeSize (Edge r1 d1, Edge r2 (pred d)))
      + index (roomRangeAdj (r1,r2) d) r


normDirRange :: (Dir,Dir)
normDirRange = (North, East)

rangeEdge :: (Room,Room) -> (Dir,Dir) -> (Edge,Edge)
rangeEdge (r0,r1) (d0,d1) = (Edge r0 d0, Edge r1 d1)

edgeMapRange, edgeFullRange :: (Edge, Edge)
edgeMapRange = rangeEdge roomRange normDirRange
edgeFullRange = rangeEdge roomRange (minBound, maxBound)

edgeCount :: Int
edgeCount = rangeSize edgeMapRange
edgeMapList, edgeFullList :: [Edge]
edgeMapList = range edgeMapRange
edgeFullList = range edgeFullRange

edgeDest :: Edge -> Room
edgeDest (Edge r d) = r |> d

roomEdges :: Room -> [Edge]
roomEdges r = map (Edge r) $ roomDirs r


type EdgeMap a = Array Edge a

type Flopper = forall a . Floppable a => a -> a

class Floppable a => Edgeable a where
  outside :: a
instance Edgeable Door where
  outside = Wall

normDir :: Dir -> Flopper
normDir d
  | inRange normDirRange d = id
  | otherwise = flop

normEdge :: Floppable a => Edge -> a -> (Edge, a)
normEdge rd@(Edge _ d) v = (f rd, f v) where
  f :: Flopper
  f = normDir d

mnormEdge :: Floppable a => Edge -> a -> Maybe (Edge, a)
mnormEdge rd
  | inRange edgeFullRange rd = Just . normEdge rd
  | otherwise = const Nothing

enormEdge :: Edgeable a => Edge -> a -> (Edge, a)
enormEdge rd = maybe (rd, outside) id . mnormEdge rd

getEdge :: Edgeable a => EdgeMap a -> Edge -> a
getEdge m r = v' where (r',v') = enormEdge r (m ! r')

setEdge :: Edgeable a => EdgeMap a -> Edge -> a -> EdgeMap a
setEdge m r v = m // [normEdge r v]

flopEdge :: Edgeable a => EdgeMap a -> Edge -> EdgeMap a
flopEdge m r = accum (const . flop) m [normEdge r (undefined :: Door)]
