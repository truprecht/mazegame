module Maze.MazeGraph (
    mazeGraph,
    mazeGraphComponents,
    mazeGraphReachability,
    policyHorizonValue,
    horizonValue
  ) where

import Data.Maybe
import Data.Array.IArray
import Data.Graph
import Maze.World
import Maze.Room
import Maze.Edge
import Maze
import Control.Arrow (first)

mazeGraphEdges :: Maze -> [(Coord, Coord, [Coord])]
mazeGraphEdges m = map rn roomList
  where
    rn :: Room -> (Room, Room, [Room])
    rn r = (r, r, mapMaybe (succRoom r) directions)
    succRoom :: Room -> Dir -> Maybe Room
    succRoom r d = if mazeDoor m (Edge r d) >= DoorInc
                      then Just (r |> d)
                      else Nothing

mazeGraph :: Maze -> Graph
mazeGraph m = graph where
  (graph, _, _) = graphFromEdges $ mazeGraphEdges m

mazeGraphComponents :: Maze -> Int
mazeGraphComponents = length . stronglyConnComp . mazeGraphEdges 

mazeGraphReachable :: Maze -> Coord -> Int
mazeGraphReachable m xy = length $ reachable g v where
  (g, _, vf) = graphFromEdges $ mazeGraphEdges m
  Just v = vf xy

mazeGraphReachability :: Maze -> Rational
mazeGraphReachability m = let reachables = map (mazeGraphReachable m) roomList
                          in fromIntegral (sum reachables) / fromIntegral (length reachables)

policyOccupancy :: EdgeMap Probability -> RoomMap Probability

policyOccupancy p = amap (/t) rp where
  rp = accumArray (+) 0 roomRange $ map (first edgeDest) $ assocs p
  t = sum $ elems rp

randOccupancy :: RoomMap Probability
randOccupancy = listArray roomRange (map ((/ fromIntegral s) . fromIntegral) d) where
  d = map (length . roomDirs) roomList
  s = sum d

occupancyHorizonValue :: Maze -> RoomMap Probability -> Value
occupancyHorizonValue m o
  = sum $ catMaybes $ zipWith (\ v p -> ((p *)) <$> v) (elems $ mazeRewards m) (elems o)

policyHorizonValue :: Maze -> EdgeMap Probability -> Value
policyHorizonValue m p = occupancyHorizonValue m (policyOccupancy p)

horizonValue :: Maze -> Value
horizonValue m = occupancyHorizonValue m randOccupancy
