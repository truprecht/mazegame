{-# LANGUAGE FlexibleInstances #-}

module Maze.Room (
    Room,
    (|-|),
    roomRange, roomCount, roomList,
    RoomMap,
    roomDirs,
    Door(..),
    doorExit
  ) where

import Data.Array.IArray
import Maze.World
import Data.Vec
import Game.Param (mazeSize)

type Room = Coord

infix |-|
(|-|) :: Room -> Room -> Int
(x1,y1) |-| (x2,y2) = abs (x1-x2) + abs (y1-y2)

roomRange :: (Room, Room)
roomRange = ((1,1), mazeSize)
roomCount :: Int
roomCount = rangeSize roomRange
roomList :: [Room]
roomList = range roomRange

roomDirs :: Room -> [Dir]
roomDirs (x,y) = 
    ifthen (y < y1) North $
    ifthen (x < x1) East $
    ifthen (y > y0) South $
    ifthen (x > x0) West [] where
  ifthen f d r 
    | f = d : r
    | otherwise = r
  ((x0,y0),(x1,y1)) = roomRange

type RoomMap a = Array Room a

instance Applicative (Array Room) where
  pure val = listArray roomRange $ repeat val
  f <*> a = listArray roomRange $ zipWith ($) (elems f) (elems a)
instance Vec (Array Room) where
  vecLength = rangeSize . bounds
  vecList = elems
  listVec = listArray roomRange
  mapV = amap

data Door = Wall | DoorDec | DoorInc | Door deriving (Eq, Ord, Enum, Show, Read)

instance Floppable Door where
  flop Wall = Wall
  flop DoorInc = DoorDec
  flop DoorDec = DoorInc
  flop Door = Door

doorExit :: Door -> Bool
doorExit Door = True
doorExit DoorInc = True
doorExit _ = False
