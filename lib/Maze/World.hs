module Maze.World (
    Coord,
    Dir(..),
    dir,
    (|>),
    Turn, turns,
    turnAhead, turnRight, turnBack, turnLeft,
    (>+),
    Time,
    Value, Probability,
    RewardParam, Reward,
    Floppable(..),
    directions,
    succ', pred'
  ) where

import Data.List
import Data.Ix (Ix)

type Coord = (Int, Int)

data Dir = North | East | South | West deriving (Eq, Ord, Enum, Bounded, Ix)

dirChrs :: [Char]
dirChrs = "NESW"

succ', pred' :: Dir -> Dir
succ' West = North
succ' d = succ d
pred' North = West
pred' d = pred d

directions :: [Dir]
directions = enumFromTo minBound maxBound

instance Show Dir where
  show d = [dirChrs !! (fromEnum d)]
instance Read Dir where
  readsPrec _ [] = []
  readsPrec i (' ':r) = readsPrec i r
  readsPrec _ (c:r) = maybe [] (\d -> [(toEnum d,r)]) $ elemIndex c dirChrs

dir :: Dir -> Coord
dir North = (0,1)
dir East = (1,0)
dir South = (0,-1)
dir West = (-1,0)

infixl 6 |>
(|>) :: Coord -> Dir -> Coord
(|>) (x,y) North = (x,succ y)
(|>) (x,y) East = (succ x,y)
(|>) (x,y) South = (x,pred y)
(|>) (x,y) West = (pred x,y)

newtype Turn = Turn Dir deriving (Eq)

turnAhead, turnBack, turnLeft, turnRight :: Turn
turnAhead = Turn North
turnRight = Turn East
turnBack = Turn South
turnLeft = Turn West
turnChrs :: [Char]
turnChrs = "^>v<"


instance Show Turn where
  show (Turn d) = [turnChrs !! (fromEnum d)]


instance Read Turn where
  readsPrec _ [] = []
  readsPrec i (' ':r) = readsPrec i r
  readsPrec _ (c:r) = maybe [] (\d -> [(Turn $ toEnum d,r)]) $ elemIndex c turnChrs

turns :: [Turn]
turns = [turnLeft, turnAhead, turnRight]

infixl 6 >+
(>+) :: Dir -> Turn -> Dir
(>+) d (Turn t) = d +% fromEnum t

enumAddMod :: (Enum a, Bounded a) => Int -> a -> a
enumAddMod i a = toEnum (b + (i + fromEnum a - b) `mod` r) where
  b = fromEnum (minBound `asTypeOf` a)
  r = 1 + fromEnum (maxBound `asTypeOf` a) - b

(+%) :: (Enum a, Bounded a) => a -> Int -> a
(+%) = flip enumAddMod

type Time = Float
type Value = Double
type Probability = Double
type RewardParam = Probability
type Reward = Value

class Floppable a where
  flop :: a -> a

instance Floppable Dir where
  -- flop = (>+ turnBack)
  flop North = South
  flop East = West
  flop South = North
  flop West = East

