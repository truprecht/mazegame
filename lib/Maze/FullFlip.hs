module Maze.FullFlip (
    initFullMaze,
    flipDoors,
    flipMany
  ) where

import Maze.World
import Maze.Room
import Maze.Edge
import Maze
import Data.Rand

initFullMaze :: Maze
initFullMaze = initGridMaze i
  where
    i (Edge (x,_) North)
      | even x = DoorInc
      | otherwise = DoorDec
    i (Edge (_,y) East)
      | even y = DoorDec
      | otherwise = DoorInc
    i (Edge (x,_) South)
      | even x = DoorDec
      | otherwise = DoorInc
    i (Edge (_,y) West)
      | even y = DoorInc
      | otherwise = DoorDec

flippable :: Maze -> Edge -> Bool
flippable m rd = case mazeDoor m rd of
                      DoorInc -> 1 < length (filter (DoorInc ==) (mazeRoomDoors m (edgeR rd)))
                      DoorDec -> 1 < length (filter (DoorInc ==) (mazeRoomDoors m (edgeDest rd)))
                      _ -> False

flippableForUser :: Maze -> (Room, Dir) -> Edge -> Bool
flippableForUser m (r', d') rd = any (doorExit . mazeDoor (mazeDoorFlip m rd) . Edge r' . (d' >+)) turns

flipif :: (Maze -> Edge -> Bool) -> Maze -> Edge -> Maze
flipif f m rd
  | f m rd = mazeDoorFlip m rd
  | otherwise = m

flipDoors :: Maze -> (Room, Dir) -> [Edge] -> Maze
flipDoors m rd = foldl (flipif (\ m' e' -> flippable m' e' && flippableForUser m' rd e')) m 


flipMany :: Maze -> RandM Maze
flipMany m = do es <- randSubset (rand (1/2 :: Rational)) edgeMapList
                return $ foldl (flipif flippable) m es