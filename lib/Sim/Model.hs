{-# LANGUAGE ImpredicativeTypes, FlexibleContexts #-}

module Sim.Model (
    Model(..),
    ModelType(..),
    modelInit,
    modelPolicy,
    modelStep,
    modelRevHist,
    decisionLikelihood,
    inits
  ) where

import Data.Array
import Maze.World
import Maze.Room
import Maze.Edge
import Game.Param
import Sim.ModelParam
import Maze
import Game
import Game.History
import Sim.Choice
import qualified Sim.Model.ModelFull            as ModelFull
import qualified Sim.Model.ModelQLA             as ModelQLA
import qualified Sim.Model.IterativeForwardPlan as ModelFP
import qualified Sim.Model.ModelRESucc          as ModelRESucc
import qualified Sim.Model.ModelRASR            as ModelRASR
import qualified Sim.Model.ModelTD              as ModelTD

import Numeric.AD
import Data.Maybe (fromMaybe)

data ModelType = ModelDumb | ModelFull | ModelTD | ModelRASR | ModelRESucc | ModelQLA | ModelFP deriving (Eq, Show, Read)
data Model a = DumbModelWrapper
             | FullModelWrapper{ innerModel :: ModelFull.FullModel a
                               , modelGetParam :: ModelFull.FullModel a -> MPof a
                               , modelLearn :: GameStep -> (Room, Dir) -> [(Dir, Door)] -> Maybe Reward -> ModelFull.FullModel a -> ModelFull.FullModel a
                               , modelEval :: ModelFull.FullModel a -> Edge -> a
                               , modelChoice :: ModelFull.FullModel a -> [a] -> [a]
                               }
             | SuccModelWrapper{ sInner :: ModelRESucc.SuccModel a
                               , sGetParam :: ModelRESucc.SuccModel a -> MPof a
                               , sLearn :: GameStep -> (Room, Dir) -> [(Dir, Door)] -> Maybe Reward -> ModelRESucc.SuccModel a -> ModelRESucc.SuccModel a
                               , sEval :: ModelRESucc.SuccModel a -> Edge -> a
                               , sChoice :: ModelRESucc.SuccModel a -> [a] -> [a]
                               }
             | TDModelWrapper{ tdInner :: ModelTD.TDModel a
                             , tdGetParam :: ModelTD.TDModel a -> MPof a
                             , tdLearn :: GameStep -> (Room, Dir) -> [(Dir, Door)] -> Maybe Reward -> ModelTD.TDModel a -> ModelTD.TDModel a
                             , tdEval :: ModelTD.TDModel a -> Edge -> a
                             , tdChoice :: ModelTD.TDModel a -> [a] -> [a]
                             }


modelInit :: (RealFloat a, Mode a, Ord a, Fractional (Scalar a), Edgeable a) => ModelType -> MPof a -> Model a
modelInit ModelDumb _ = DumbModelWrapper
modelInit ModelFull p = FullModelWrapper{ innerModel = ModelFull.modelInit p
                                        , modelGetParam = ModelFull.modelParam
                                        , modelLearn = ModelFull.modelLearn
                                        , modelEval = ModelFull.modelEval
                                        , modelChoice = ModelFull.modelChoice
                                        }
modelInit ModelQLA p = (modelInit ModelFull p){modelEval = ModelQLA.modelEval}
modelInit ModelFP p = (modelInit ModelFull p){modelEval = ModelFP.modelEval}
modelInit ModelRESucc p = SuccModelWrapper{ sInner = ModelRESucc.modelInit p
                                          , sGetParam = ModelRESucc.modelParam
                                          , sLearn = ModelRESucc.modelLearn
                                          , sEval = ModelRESucc.modelEval
                                          , sChoice = ModelRESucc.modelChoice
                                          }
modelInit ModelRASR p = (modelInit ModelRESucc p){ sLearn = ModelRASR.modelLearn }
modelInit ModelTD p = TDModelWrapper{ tdInner = ModelTD.modelInit p
                                    , tdGetParam = ModelTD.modelParam
                                    , tdLearn = ModelTD.modelLearn
                                    , tdEval = ModelTD.modelEval
                                    , tdChoice = ModelTD.modelChoice
                                    }


modelValues' :: (Edge -> a) -> EdgeMap a
modelValues' f = listArray edgeFullRange $ map f $ range edgeFullRange

modelValues :: (Num a) => Model a -> EdgeMap a
modelValues DumbModelWrapper{} = modelValues' (const 0)
modelValues FullModelWrapper{ modelEval = me, innerModel = im } = modelValues' (me im)
modelValues SuccModelWrapper{ sEval = me, sInner = im } = modelValues' (me im)
modelValues TDModelWrapper{ tdEval = me, tdInner = im } = modelValues' (me im)


modelPolicyWith' :: (Num a) => (Room -> [Edge]) -> ([a] -> [a]) -> (Edge -> a) -> EdgeMap a
modelPolicyWith' f choiceFunc eval = accumArray (flip const) 0 edgeFullRange
                                   $ concatMap rpl roomList
  where
    --rpl :: Room -> [(Edge, a)]
    rpl r = let e = f r
            in  zip e $ choiceFunc $ map eval e
          
modelPolicyWith :: (RealFloat a) => (Room -> [Edge]) -> Model a -> EdgeMap a
modelPolicyWith f DumbModelWrapper = modelPolicyWith' f (choice 0) (const 0)
modelPolicyWith f FullModelWrapper{ innerModel = im, modelChoice = c, modelEval = e } = modelPolicyWith' f (c im) (e im)
modelPolicyWith f SuccModelWrapper{ sInner = im, sChoice = c, sEval = e } = modelPolicyWith' f (c im) (e im) 
modelPolicyWith f TDModelWrapper{ tdInner = im, tdChoice = c, tdEval = e } = modelPolicyWith' f (c im) (e im)
  


modelPolicy :: (RealFloat a) => Model a -> EdgeMap a
modelPolicy = modelPolicyWith roomEdges


modelStep :: (RealFloat a, Mode a, Num (Scalar a)) => Model a -> Game -> (Model a, [(Dir, (a, a))])
modelStep m@DumbModelWrapper{} Game{ gameMaze = mz, gamePos = (r,d) } 
  = (m, zip el $ zip vl cl )
    where
      cl = choice (auto 0) vl
      vl = map (const (auto 0)) el
      el = filter (doorExit . mazeDoor mz . Edge r) $ map (d >+) turns
modelStep mw@FullModelWrapper{} Game{ gameStep = gs, gameMaze = mz, gamePos = rd@(r,d), gameReward = rr }
  = (mw{ innerModel = im' }, zip el $ zip vl cl)
    where
      im' = modelLearn mw gs rd [(dir', df dir') | dir' <- dirl] rr (innerModel mw)
      vf = modelEval mw im' . Edge r
      cl = modelChoice mw im' vl
      vl = map vf el
      el = filter (doorExit . df) dirl
      df = mazeDoor mz . Edge r
      dirl = map (d >+) turns
modelStep mw@SuccModelWrapper{} Game{ gameStep = gs, gameMaze = mz, gamePos = rd@(r,d), gameReward = rr }
  = (mw{ sInner = im' }, zip el $ zip vl cl)
    where
      im' = sLearn mw gs rd [(dir', df dir') | dir' <- dirl] rr (sInner mw)
      vf = sEval mw im' . Edge r
      cl = sChoice mw im' vl
      vl = map vf el
      el = filter (doorExit . df) dirl
      df = mazeDoor mz . Edge r
      dirl = map (d >+) turns
modelStep mw@TDModelWrapper{} Game{ gameStep = gs, gameMaze = mz, gamePos = rd@(r,d), gameReward = rr }
  = (mw{ tdInner = im' }, zip el $ zip vl cl)
    where
      im' = tdLearn mw gs rd [(dir', df dir') | dir' <- dirl] rr (tdInner mw)
      vf = tdEval mw im' . Edge r
      cl = tdChoice mw im' vl
      vl = map vf el
      el = filter (doorExit . df) dirl
      df = mazeDoor mz . Edge r
      dirl = map (d >+) turns


modelRevHist :: ModelType -> ModelParam -> [History] -> ([(Dir, (Value, Probability))], EdgeMap Value)
modelRevHist mt p h = (dirValues, modelValues m)
  where
    (m, dirValues) = foldl (learnFromHistoryStep) (modelInit mt p, []) $ map histGame $ filter isHistory h
    learnFromHistoryStep (prevModel, _) = modelStep prevModel


-- modelParamSplit :: ModelType -> MPof a -> ([a], [a])
-- modelParamSplit ModelDumb mp = (toList mp, [])
-- modelParamSplit ModelTD mp = ([mpDoorDecay mp, mpDoorRate mp, mpDepth mp], [mpTemp mp, mpDiscount mp, mpValueInit mp, mpValueRate mp, mpLambda mp])
-- modelParamSplit ModelQLA mp = ([mpValueInit mp, mpValueRate mp, mpLambda mp, mpDoorRate mp, mpDepth mp], [mpTemp mp, mpDiscount mp, mpDoorDecay mp])

-- modelParamFill :: ModelType -> [Double]
-- modelParamFill ModelDumb = replicate 8 0
-- modelParamFill ModelTD   = replicate 3 0
-- modelParamFill ModelQLA  = replicate 5 0

-- modelParamUnSplit :: ModelType -> [a] -> [a] -> MPof a
-- modelParamUnSplit ModelDumb ps [] = fromList ps
-- modelParamUnSplit ModelTD [x2, x3, x8] [x7, x1, x5, x4, x6] = fromList [x1, x2, x3, x4, x5, x6, x7, x8]
-- modelParamUnSplit ModelQLA [x5, x4, x6, x3, x8] [x7, x1, x2] = fromList [x1, x2, x3, x4, x5, x6, x7, x8]


inits :: ModelType -> MPof Double
inits ModelDumb = fromList $ replicate 8 0
inits ModelTD = MP 0.861 (2/fromIntegral edgeCount) 1 0.408 2.883 0.756 5.315 0
inits ModelQLA = MP 0.816 (2/fromIntegral edgeCount) 1 0.2 (0.5 * fromIntegral rewardCount / fromIntegral roomCount) 0 11.78 16


decisionLikelihood :: [[History]] -> ModelType -> ModelParam -> (Double, ModelParam)
decisionLikelihood hss mt = grad' (negate . sum . map log . concat . (map decisionProbabilities hss <*>) . return)
-- decisionLikelihood hss mt mp = (likelihood, modelParamUnSplit mt (modelParamFill mt) gradient)
--   where
--     (fixed, fitted) = modelParamSplit mt mp
--     (likelihood, gradient) = grad' (negate . sum . map log . concat . (map decisionProbabilities hss <*>) . return) fitted
  where
    decisionProbabilities :: (RealFloat a, Mode a, Fractional (Scalar a), Edgeable a, Show a) => [History] -> MPof a -> [a]
    decisionProbabilities hs mp = snd $ foldl (flip decisionStep) (modelInit mt mp, []) (histDecisions hs)
    
    decisionStep :: (RealFloat a, Show a, Mode a, Num (Scalar a)) => (Game, Dir) -> (Model a, [a]) -> (Model a, [a])
    decisionStep (gameState, decision) (prevModel, ps)
      = let (currentModel, probList) = modelStep prevModel gameState
        in (currentModel, snd (fromMaybe (auto 1, auto 1) $ lookup decision probList) : ps)  
