{-# LANGUAGE TemplateHaskell, BangPatterns #-}
{-# OPTIONS_GHC -fno-warn-missing-fields #-}
module Sim.ModelParam where

import Data.Vec
import Data.Rand (rand, RandM())
import Maze.Room (roomCount)
import Maze.Edge (edgeCount)
import Game.Param
import Data.List (elemIndex)
import Numeric.Limits (minValue, maxValue)

data MPof a = MP !a !a !a !a !a !a !a !a deriving (Eq)

mpDiscount, mpDoorDecay, mpDoorRate, mpValueRate, mpValueInit, mpLambda, mpTemp, mpDepth :: MPof a -> a
mpDiscount (MP d _ _ _ _ _ _ _) = d   -- gamma
mpDoorDecay (MP _ d _ _ _ _ _ _) = d  -- decay parameter
mpDoorRate (MP _ _ d _ _ _ _ _) = d   -- update parameter on seen (should converge to 1)
mpValueRate (MP _ _ _ d _ _ _ _) = d  -- alpha
mpValueInit (MP _ _ _ _ d _ _ _) = d  -- Q_0
mpLambda (MP _ _ _ _ _ d _ _) = d     -- td trace 
mpTemp (MP _ _ _ _ _ _ d _) = d       -- beta (softmax multiplicator)
mpDepth (MP _ _ _ _ _ _ _ d) = d


type MP = MPof Double
type ModelParam = MP


fromList :: [a] -> MPof a
fromList [x1, x2, x3, x4, x5, x6, x7, x8] = MP x1 x2 x3 x4 x5 x6 x7 x8
fromList _ = error "ModelParam: Using fromList with a List of wrong length."
toList :: MPof a -> [a]
toList (MP x1 x2 x3 x4 x5 x6 x7 x8) = [x1, x2, x3, x4, x5, x6, x7, x8]


instance Functor MPof where
  fmap f = fromList . map f . toList

instance Applicative MPof where
  pure = fromList . replicate 8
  fs <*> m = fromList $ zipWith ($) (toList fs) (toList m)

instance Foldable MPof where
  foldMap f = foldMap f . toList

instance Traversable MPof where
  traverse f = fmap fromList . traverse f . toList

instance Vec MPof where
  vecList = toList
  listVec = fromList


mpNames :: MPof String
mpNames = MP "discount" "door-decay" "door-rate" "value-rate" "value-init" "lambda" "temp" "depth"

updateParameter :: MPof a -> String -> a -> MPof a
updateParameter ps pname pvalue = maybe ps (fromList . alterIndex (const pvalue) (toList ps)) maybeIndex
  where
    maybeIndex = pname `elemIndex` toList mpNames
    alterIndex :: (a -> a) -> [a] -> Int -> [a]
    alterIndex f (a:as) 0 = f a : as
    alterIndex f (a:as) i = a : alterIndex f as (i-1)
    alterIndex _ [] _ = []

instance Show a => Show (MPof a) where
  show p = "{" ++ unwords (zipWith (\n p' -> n ++ "=" ++ show p') (vecList mpNames) (vecList p)) ++ "}"

updateFromString :: (Read a) => MPof a -> String -> MPof a
updateFromString origParam str = foldl update origParam (words str)
  where
    update origParam' paramString = case break (== '=') paramString of
                                        (pname, '=':pval) -> updateParameter origParam' pname (read pval)
                                        _ -> error $ "Incorrect pattern while parsing parameter values (" ++ show paramString ++ ")."

initModelParam :: MPof Double
initModelParam = MP (fromRational $ 1 - warpProb) (2/fromIntegral edgeCount) 1 0.2 (0.5 * fromIntegral rewardCount / fromIntegral roomCount) 0 4 16

validRanges, sampleRanges :: (Num a, Ord a, RealFloat a) => MPof (a, a)
validRanges = MP (0, 1) (0, 1) (0, 1) (minValue, 1) (-maxValue, maxValue) (0, 1) (-maxValue, maxValue) (0, maxValue)
sampleRanges = MP (0, 1) (0, 1) (0, 1) (minValue, 1) (0, 10) (0, 1) (0, 20) (16, 16)

proper :: (Num a, Ord a, RealFloat a) => MPof a -> MPof a
proper m = pushBoundaries <$> m <*> validRanges
  where
    pushBoundaries :: Ord b => b -> (b, b) -> b
    pushBoundaries value (mini, maxi) = min (max value mini) maxi


randomParam :: RandM ModelParam
randomParam = mapM (rand :: (Double, Double) -> RandM Double) sampleRanges