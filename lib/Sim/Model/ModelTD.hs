-- Temporal difference model --
{-# LANGUAGE FlexibleContexts #-}

module Sim.Model.ModelTD (
    TDModel(..),
    modelInit,
    modelEval,
    modelLearn,
    modelChoice
  ) where

import Data.Array
import Maze.World
import Maze.Room
import Maze.Edge
import Sim.ModelParam
import Sim.Choice
import Game

import Numeric.AD

-- | model structures for temporal difference model
data TDModel a = TDModel {
  modelParam :: MPof a,       -- model parameters
  modelValues :: RoomMap a,   -- maps each room to value
  modelTraces :: [Room]       -- list of last visited rooms
}

-- | initializes the model structure in the beginning
modelInit :: MPof a -> TDModel a
modelInit p = TDModel {
    modelParam = p,                                             -- stores parameters
    modelValues = listArray roomRange $ repeat $ mpValueInit p, -- each room is mapped to the initial value Q_0
    modelTraces = []                                            -- there are no visited rooms, yet
  }

-- | learns the model in each step
modelLearn :: (Fractional a, Mode a, Fractional (Scalar a), Ord a) => GameStep -> (Room,Dir) -> [(Dir,Door)] -> Maybe Reward -> TDModel a -> TDModel a
modelLearn gs (r,_) dd rr m@(TDModel { modelParam = p, modelValues = mrv, modelTraces = et })
  = m{ modelValues = mrv', modelTraces = et' }  -- returns updated model structure with updated map and updated traces
    where
      mrv' = accum (+) mrv $ zip et' updates    -- updates the `modelValues` map:
                                                -- adds to each room in the traces list the corresponding value from the `updates` list
      et' = r : if gs == StepWarp               -- prepends the current room to the traces list
                   then [] 
                   else et
      
      -- calculates the delta value using
      delta = negate (mrv ! r)                                                                              -- negative value of the current room
              + (auto . maybe 0 realToFrac) rr                                                              -- the reward of the current room
              + mpDiscount p * maximum (case map ((mrv !) . (r |>) . fst) $ filter (doorExit . snd) dd of   -- discount * maximum value over all successor rooms
                                             [] -> [auto 0]                                                 -- (0 if there is no successor room)
                                             xs -> xs)
      
      -- | builds an infinite list 
      -- [valueRate * delta, discount * lambda * valueRate * delta, (discount * lambda)^2 * valueRate * delta, ...]
      updates = iterate (mpDiscount p * mpLambda p *) (mpValueRate p * delta)

-- the value of a door is the map value of its destination
modelEval :: TDModel a -> Edge -> a
modelEval m r = modelValues m ! edgeDest r

-- uses the softmax decision with the temperature parameter
modelChoice :: (RealFloat a) => TDModel a -> [a] -> [a]
modelChoice = choice . mpTemp . modelParam