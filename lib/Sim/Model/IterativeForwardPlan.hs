-- iterative implementation of forward plan model --
{-# LANGUAGE TypeSynonymInstances, FlexibleContexts #-}

module Sim.Model.IterativeForwardPlan (
    modelEval
  ) where

import Data.List
import Data.Array.IArray
import Maze.World
import Maze.Edge
import Sim.ModelParam
import Sim.Model.ModelFull hiding (modelEval)

modelDoor :: (Edgeable a) => FullModel a -> Edge -> a
modelDoor m = getEdge (modelDoors m)

type QValues a = EdgeMap a

iterateQValues :: (Edgeable a, Num a, Ord a) => FullModel a -> QValues a -> QValues a
iterateQValues m v = listArray edgeFullRange [ successorValue edge
                                             | edge <- range edgeFullRange
                                             ]
  where
    successorValue e = modelDoor m e * case modelRewards m ! (edgeDest e) of
                                            0 -> mpDiscount (modelParam m) * maximum (map (v !) (successorEdges e))
                                            reward -> reward
    successorEdges e = filter (inRange edgeFullRange)
                     $ map (Edge (edgeDest e))
                     $ delete (flop $ edgeD e) directions

initQValues :: (Edgeable a) => QValues a
initQValues = listArray edgeFullRange $ repeat outside

modelEval :: (RealFrac a, Ord a, Edgeable a) => FullModel a -> Edge -> a
modelEval m = ev
  where
    (d,f0) = properFraction $ mpDepth $ modelParam m
    qv1:qv2:_ = drop d $ iterate (iterateQValues m) initQValues
    ev e = (1-f0) * (qv1 ! e) + f0 * (qv2 ! e)
