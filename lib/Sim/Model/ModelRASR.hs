-- the RASR model: room action successor room
-- This is the Successor representation (Dayan 93) with no door knowledge and only immediate reward updates
{-# LANGUAGE FlexibleContexts #-}

module Sim.Model.ModelRASR (
    modelLearn
  ) where

import Data.Array.IArray
import Maze.World
import Maze.Room
import Sim.ModelParam
import Game
import Control.Applicative
import Sim.Model.ModelRESucc hiding (modelLearn)

import Numeric.AD


update :: (Num a) => a -> a -> a -> a
update r x u = x + r * (u - x)

modelLearn :: (Fractional a, Mode a, Fractional (Scalar a)) => GameStep -> (Room,Dir) -> [(Dir,Door)] -> Maybe Reward -> SuccModel a -> SuccModel a
modelLearn gs (r,_) _ rr m@SuccModel{ modelParam = p, modelValues = mrv, modelSucc = ms, modelTrace = mt }
  = m{ modelValues = mrv', modelSucc = ms', modelTrace = r:mt' }
    where
      mt' = if gs == StepWarp then [] else mt
      mrv' = accum (update (mpValueRate p)) mrv [(r, maybe 0 (auto . realToFrac) rr)]
      ms' = case mt' of
                 [] -> ms
                 r1:_ -> let msr = ms ! r
                             rs = msr // [(r1, (msr ! r1) + 1)]
                             upd u x = update (mpDoorRate p) x (mpDiscount p * u)
                         in ms // [(r1, liftA2 upd rs (ms ! r1))]