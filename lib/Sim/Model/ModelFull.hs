{-# LANGUAGE TypeSynonymInstances, FlexibleContexts, TypeOperators #-}

module Sim.Model.ModelFull (
    FullModel(..),
    modelInit,
    modelChoice,
    modelLearn,
    modelEval
  ) where

import Data.Maybe
import Data.Array.IArray
import Maze.World
import Maze.Room
import Maze.Edge
import Sim.ModelParam
import Sim.Choice

import Numeric.AD
import Numeric.AD.Mode.Reverse (Reverse)
import Numeric.AD.Internal.Reverse (Tape)
import Data.Reflection (Reifies)

instance Floppable Double where
  flop x = 1-x
instance (Reifies s Tape, Num a) => Floppable (Reverse s a) where
  flop x = 1-x
instance Edgeable Double where
  outside = 0
instance (Reifies s Tape, Num a) => Edgeable (Reverse s a) where
  outside = 0

-- model structure for forward planing models
data FullModel a = GridModel {
  modelParam :: MPof a,       -- stores the model parameters
  modelDoors :: EdgeMap a,    -- a door probability model
  modelRewards :: RoomMap a   -- a reward model for each room
}

-- initializes the model
modelInit :: (Fractional a) => MPof a -> FullModel a
modelInit p = GridModel {
    modelParam = p,                                             -- stores the parameters
    modelDoors = listArray edgeMapRange $ repeat 0.5,           -- initializes p(*open* | door) = p(*closed* | door) = 0.5 for each door
    modelRewards = listArray roomRange $ repeat $ mpValueInit p -- initializes each room with expected reward value of Q_0
  }

-- value update weighted with old value
updateDoorProbability :: (Fractional a) => a -> a -> a -> a
updateDoorProbability parameter oldvalue newvalue = oldvalue + parameter * (newvalue - oldvalue) -- = (parameter * newvalue + (1- parameter) * oldvalue)

modelDoor :: (Edgeable a) => FullModel a -> Edge -> a
modelDoor m = getEdge (modelDoors m)

modelDoorSet :: (Edgeable a) => FullModel a -> Edge -> a -> FullModel a
modelDoorSet m@GridModel{ modelDoors = md } r v = m{ modelDoors = setEdge md r v }

observeMazeRoom :: (Fractional a) => Room -> [(Dir,Door)] -> FullModel a -> FullModel a
observeMazeRoom r dd m@GridModel{ modelDoors = mds }
  = m{ modelDoors = obs mds }
    where
      obs md = accum updateDoorModel md $ mapMaybe dp dd
      
      dp (d,e) = mnormEdge (Edge r d) e
      
      updateDoorModel md DoorInc = updateDoorProbability (mpDoorRate $ modelParam m) md 1
      updateDoorModel md DoorDec = updateDoorProbability (mpDoorRate $ modelParam m) md 0
      updateDoorModel _ _ = error "FullModel: Observed wall or open door."

decayDoors :: (Fractional a) => FullModel a -> FullModel a
decayDoors m@GridModel{ modelDoors = mds } = m{ modelDoors = amap decay mds }
  where
    decay p = updateDoorProbability (mpDoorDecay $ modelParam m) p 0.5

observeReward :: (Fractional a, Mode a, Fractional (Scalar a)) => Room -> Maybe Reward -> FullModel a -> FullModel a
observeReward r rr m@GridModel{ modelRewards = mrs } = m{ modelRewards = mrs // [(r, reward)] }
  where
    reward = maybe 0 (updateDoorProbability (mpValueRate $ modelParam m) (mrs ! r) . auto . realToFrac) rr

-- model learning step:
-- * decays door probabilities using doorDecay parameter
-- * updates door probabilities of currently seen doors using doorRate parameter
-- * updates reward map for the current room using valueRate parameter
modelLearn :: (Fractional a, Mode a, Fractional (Scalar a)) => b -> (Room,Dir) -> [(Dir,Door)] -> Maybe Reward -> FullModel a -> FullModel a
modelLearn _ (r,_) dd rr = observeReward r rr . observeMazeRoom r dd . decayDoors


evaluateRoomValue :: (Fractional a, Ord a, Edgeable a) => Int -> FullModel a -> a -> Room -> a
evaluateRoomValue l m@GridModel{ modelParam = p } d r 
  | ld <= 1    = ld * d * rr
  | otherwise  = d * rr + maximum (evaluateDoorValues (succ l) (decayDoors m) (d * mpDiscount p) r)
  where
    ld = mpDepth p - fromIntegral l
    rr = modelRewards m ! r


evaluateDoorValues :: (Fractional a, Ord a, Edgeable a) => Int -> FullModel a -> a -> Room -> [a]
evaluateDoorValues l m d r = map (evaluateDoorValue l m d . Edge r) directions

evaluateDoorValue :: (Edgeable a, Fractional a, Ord a) => Int -> FullModel a -> a -> Edge -> a
evaluateDoorValue l m d r = 
  case md of
    0 -> 0
    1 -> evaluateRoomValue l m                    (d * md) (edgeDest r)
    _ -> evaluateRoomValue l (modelDoorSet m r 1) (d * md) (edgeDest r)
  where md = modelDoor m r

modelEval :: (Fractional a, Ord a, Mode a, Num (Scalar a), Edgeable a) => FullModel a -> Edge -> a
modelEval m = evaluateDoorValue 0 m (auto 1)

modelChoice :: (RealFloat a) => FullModel a -> [a] -> [a]
modelChoice = choice . mpTemp . modelParam