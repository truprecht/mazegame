-- the RESucc model: room edge successor
-- This is the Successor representation (Dayan 93) with one-step door knowledge and only immediate reward updates
{-# LANGUAGE FlexibleContexts #-}

module Sim.Model.ModelRESucc (
    SuccModel(..),
    modelInit,
    modelEval,
    modelLearn,
    modelChoice
  ) where

import Data.Array
import Maze.World
import Maze.Room
import Maze.Edge
import Sim.ModelParam
import Sim.Choice
import Game
import Control.Applicative

import Numeric.AD

data SuccModel a = SuccModel {
  modelParam :: MPof a,
  modelValues :: RoomMap a,
  modelSucc :: RoomMap (RoomMap a),
  modelTrace :: [Room]
}

modelInit :: (Num a) => MPof a -> SuccModel a
modelInit p = SuccModel {
    modelParam = p,
    modelValues = listArray roomRange $ repeat $ mpValueInit p,
    modelSucc = listArray roomRange [ listArray roomRange [ mpDiscount p ^ (room1 |-| room2) | room2 <- range roomRange]
                                    | room1 <- range roomRange
                                    ],
    modelTrace = []
  }

update :: (Fractional a) => a -> a -> a -> a
update r x u = x + r * (u - x)

succValue :: (Num a) => SuccModel a -> Room -> a
succValue (SuccModel{ modelValues = mrv, modelSucc = ms }) r 
  = sum $ zipWith (*) (elems mrv) (elems $ ms ! r)

modelLearn :: (Fractional a, Mode a, Fractional (Scalar a)) => GameStep -> (Room,Dir) -> [(Dir,Door)] -> Maybe Reward -> SuccModel a -> SuccModel a
modelLearn gs (r,_) dd rr m@(SuccModel { modelParam = p, modelValues = mrv, modelSucc = ms, modelTrace = mt })
  = m{ modelValues = mrv', modelSucc = ms', modelTrace = mt' }
    where
      mt' = r:if gs == StepWarp then [] else mt
      mrv' = accum (update (mpValueRate p)) mrv [(r, maybe 0 (auto . realToFrac) rr)]
      ms' = ms // [(r, liftA2 upd rs (ms ! r))]
      upd u x = update (mpDoorRate p) x (mpDiscount p * u)
      --sr :: [RoomMap a]
      sr = map ((ms !) . edgeDest . Edge r . fst) $ filter (doorExit . snd) dd
      --rs :: RoomMap a
      rs = let a' = (/ (fromIntegral $ length sr)) <$> foldl1 (liftA2 (+)) sr
           in a' // [(r, 1 + (a' ! r))]

modelEval :: (Num a) => SuccModel a -> Edge -> a
modelEval m = succValue m . edgeDest


modelChoice :: (RealFloat a, Ord a) => SuccModel a -> [a] -> [a]
modelChoice = choice . mpTemp . modelParam