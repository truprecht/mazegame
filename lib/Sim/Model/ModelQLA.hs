-- fixed depth forward plan model: like forward plan, but does not stop on reward --
{-# LANGUAGE TypeSynonymInstances, FlexibleContexts #-}

module Sim.Model.ModelQLA (
    modelEval
  ) where

import Data.List
import Data.Array.IArray
import Maze.World
import Maze.Edge
import Sim.ModelParam
import Sim.Model.ModelFull hiding (modelEval)

-- the model structure and modelLearn is in `ModelFull`

modelDoor :: (Edgeable a) => FullModel a -> Edge -> a
modelDoor m = getEdge (modelDoors m)

-- the q-values used for the decision, later
-- it maps each door to a real value
type QValues a = EdgeMap a


-- | iterates the map of Q-values, i.e. it returns an updated map using an old map:
-- each door is mapped to the value:
--  probability of this door being open * (reward value for the successor room + discount * maximum Q-value (old map) over the three other doors in the successor room)
iterateQValues :: (Edgeable a, Num a, Ord a) => FullModel a -> QValues a -> QValues a
iterateQValues m v = listArray edgeFullRange [ev (1 :: Int) edge | edge <- range edgeFullRange]
  where
    ev l e
      | inRange edgeFullRange e = ev' l e
      | otherwise = 0
    ev' 0 e = v ! e
    ev' l e = let doorProb = modelDoor m e
                  r = edgeDest e
                  rv = modelRewards m ! r                                                                                     -- reward value from `modelReward` for the room behind the door
                     + mpDiscount (modelParam m) * maximum (map (ev (pred l) . Edge r) $ delete (flop $ edgeD e) directions)  -- + discount * maximum Q-value over the three other doors in the successor room
              in rv `seq` doorProb `seq` (doorProb * rv)     -- probability of this door being open * `rv` value from above


-- initial map values are 0 for each door
initQValues :: (Edgeable a) => QValues a
initQValues = listArray edgeFullRange $ repeat outside


-- the decision value for a door is the value of the map after `depth` iterations of `iterateQValues`
-- starting with `initQValues`
-- if `depth` is a fraction, use the both nearest iterations and add the resulting values
modelEval :: (RealFrac a, Ord a, Edgeable a) => FullModel a -> Edge -> a
modelEval m = ev
  where
    d0 = mpDepth $ modelParam m
    (d,f0) = properFraction d0
    qv1:qv2:_ = drop d $ iterate (iterateQValues m) initQValues
    ev e = (1-f0) * (qv1 ! e) + if f0 > 0 then f0 * (qv2 ! e) else 0
