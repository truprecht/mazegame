module Sim.Choice (
    choice
  ) where

softmax :: (Floating a) => a -> [a] -> [a]
softmax p vs = let expv = map (exp . (*) p) vs
                   total = sum expv
               in map (/ total) expv

greedyChoice :: (Fractional a, Ord a) => [a] -> [a]
greedyChoice vl = map p vl
  where
    m = maximum vl
    n = length (filter (== m) vl)
    p v
      | v == m = recip (fromIntegral n)
      | otherwise = 0


dumbChoice :: (Fractional a) => [b] -> [a]
dumbChoice vl = replicate n (recip (fromIntegral n))
  where
    n = length vl

choice :: (RealFloat a, Ord a) => a -> [a] -> [a]
choice _ [_] = [1]
choice p vs
  | p <= 0 = dumbChoice vs
  | isInfinite p = greedyChoice vs
  | otherwise = softmax p vs
