module Data.GlobalIORef 
    ( module Data.IORef
    , module Data.GlobalIORef
    ) where

import Data.IORef
import System.IO.Unsafe

globalIORef :: a -> IORef a
globalIORef = unsafePerformIO . newIORef