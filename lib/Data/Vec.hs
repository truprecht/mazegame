module Data.Vec (
    Applicative(..),
    Vec(..),
    Id(..),

    (+:), (-:),
    (*:), (/:)
  ) where

import Control.Applicative
import Control.Arrow (first)

class Applicative v => Vec v where
  vecLength :: v a -> Int
  vecList :: v a -> [a]
  listVec :: [a] -> v a

  allV :: a -> v a
  mapV :: (a -> b) -> v a -> v b
  zipV :: (a -> b -> c) -> v a -> v b -> v c

  vecLength = length . vecList
  mapV = fmap
  allV = pure
  zipV = liftA2

infixl 6 +:, -:
infixl 7 *:, /:
(+:), (-:), (*:) :: (Vec v, Num a) => v a -> v a -> v a
(/:) :: (Vec v, Fractional a) => v a -> v a -> v a
(+:) = zipV (+)
(-:) = zipV (-)
(*:) = zipV (*)
(/:) = zipV (/)

newtype Id a = Id { unId :: a } deriving (Eq)
instance Show a => Show (Id a) where
  showsPrec p (Id a) = showsPrec p a
instance Read a => Read (Id a) where
  readsPrec p = map (first Id) . readsPrec p
instance Functor Id where
  fmap f (Id a) = Id (f a)
instance Applicative Id where
  pure = Id
  Id f <*> Id a = Id (f a)
instance Vec Id where
  vecLength _ = 1
  vecList (Id a) = [a]
  listVec ~(a:_) = Id a

instance Vec [] where
  vecLength = length
  vecList = id
  listVec = id
