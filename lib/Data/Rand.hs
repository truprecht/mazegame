{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts #-}

module Data.Rand (
    RandM, runSeed, runRand, runStdRand,
    rand,
    randFork,
    randShuffle,
    randSubset,
    randIO,
    (%)
  ) where

import qualified Control.Monad.State as S
import Control.Monad
import System.Random
import Data.Ratio

useState :: (r -> (a, r)) -> S.State r a
useState g = S.state g

getState :: ((r -> (a, r)) -> b) -> S.State r a -> b
getState f state = f (S.runState state)

type RandM a = S.State StdGen a

class Rand a b where
  rand :: a -> RandM b

instance (Random a) => Rand () a where
  rand () = useState $ random
instance (Random a) => Rand (a, a) a where
  rand r = useState $ randomR r

instance Rand [a] a where
  rand [] = error "Rand: empty list"
  rand l = (l !!) <$> rand (0 :: Int, pred $ length l)

instance Rand Rational Bool where
  rand p 
    | p <= 0 = return False
    | p >= 1 = return True
    | otherwise = (<= numerator p) <$> rand (1 :: Integer, denominator p)
instance Rand Double Bool where
  rand p
    | p <= 0 = return False
    | p >= 1 = return True
    | otherwise = (<= p) <$> rand ()

instance (Rand () p, RealFrac p) => Rand [(a,p)] a where
  rand l = pick l <$> rand ()
    where
      pick [] _ = error "bad distribution"
      pick ((v,p):l') x
        | x < p = v
        | otherwise = pick l' (x-p)

randFork :: (Rand p Bool) => p -> a -> a -> RandM a
randFork p t f = (\ isLeft -> if isLeft then t else f) <$> rand p

randShuffle :: [a] -> RandM [a]
randShuffle l = s (length l) l
  where
    s 0 [] = return []
    s n l' = do i <- rand (0 :: Int, pred n)
                let (x,r) = pull i l'
                (x:) <$> s (pred n) r

pull :: Int -> [a] -> (a, [a])
pull 0 (x:l) = (x, l)
pull n (x:xs) = case pull (pred n) xs of
                     (x', xs') -> (x', x:xs')
pull _ [] = error "pull: index too large"

randSubset :: RandM Bool -> [a] -> RandM [a]
randSubset p l = filterM (const p) l >>= randShuffle

--runRand :: Random.RandomGen r => r -> RandM a -> a
runRand :: StdGen -> RandM a -> a
runRand r m = S.evalState m r
runSeed :: Int -> RandM a -> a
runSeed s = runRand (mkStdGen s)
runStdRand :: RandM a -> IO a
runStdRand = getState getStdRandom

randIO :: Rand a b => a -> IO b
randIO = runStdRand . rand

instance RandomGen () where
  next r = (0,r)
  split r = (r,r)
  genRange _ = (0,0)
