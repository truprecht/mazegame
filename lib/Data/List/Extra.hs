module Data.List.Extra
    ( ppNestedList
    ) where

import Data.List (intercalate)

ppNestedList :: (Show a) => [[a]] -> String
ppNestedList xss = "[\t" 
                    ++ intercalate "\n,\t" [ "[\t" ++ intercalate "\n\t,\t" (map show xs) ++ "\n\t]" | xs <- xss ] 
                    ++ "\n]"