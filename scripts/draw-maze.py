#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from optparse import OptionParser
from sys import stdin

def from_string(maze_dump):
    maze = []
    for (door, (x, y, direction)) in zip(maze_dump, [ (i, j, "north") for i in range(1,5) for j in range(1,4) ] + [ (i, j, "east") for i in range(1,4) for j in range(1,5) ]):
        maze.append( (door == '+', x, y, direction) )
    return maze

def query(maze, x, y, direction):
    negate = False
    if direction == "south":
        negate = True
        direction = "north"
        y -= 1
    elif direction == "west":
        negate = True
        direction = "east"
        x -= 1
    
    assert x > 0 and x < 5 and y > 0 and y < 5, "Invalid position"

    for (isopen, x_, y_, dir_) in maze:
        if x_ == x and y_ == y and dir_ == direction:
            return negate != isopen 

def to_charart(m):
    string = ""

    # draw top
    top = ""
    for x in range(1,5):
        top += "---┳"
    string += "┏" + top[0:-1] + "┓\n"

    # draw doors to east and north
    for y in range(4,0,-1):
        if y < 4:
            row = ""
            for x in range(1,5):
                row += " %s ╋" %("↑" if query(m, x, y, "north") else "↓",)
            string += "┣" + row[0:-1] + "┫\n"
        
        row = ""
        for x in range(1,4):
            row += "   %s" %("→" if query(m, x, y, "east") else "←",)
        string += "|" + row + "   |\n"

    # last line
    bottom = ""
    for x in range(1,5):
        bottom += "---┻"
    string += "┗" + bottom[0:-1] + "┛\n"

    return string

if __name__ == "__main__":
    options = OptionParser( usage = "Usage: %prog [options] (MAZE STRING)" )
    options.add_option("-a", "--action", action = "store", dest = "action", type = "string", default = "draw")
    options.add_option("-m", "--maze", action = "store", dest = "maze", type = "string", default = None)
    options.add_option("-p", "--position", action = "store", dest = "position", type = "string", default = None)

    (opt, _) = options.parse_args()

    if opt.maze is None:
        maze_string = stdin.readline().strip()
    else:
        maze_string = opt.maze

    assert len(maze_string) == 24, "Invalid maze definition"
    maze = from_string(maze_string)

    if opt.action == "draw":
        print(to_charart(maze))
    
    elif opt.action == "assoc":
        print("x\ty\tdirection\tstatus")
        for (is_open, x, y, dire) in maze:
            print("%d\t%d\t%s\t%s" %(x, y, dire, "open" if is_open else "closed"))
    
    elif opt.action == "query":
        assert opt.position is not None and len(opt.position) > 0, "Invalid position"
        x, y, direction = opt.position.strip().split(",")
        print("open" if query(maze, int(x), int(y), direction) else "closed")

    else:
        raise Exception("Unknown action: %s" %(opt.action,))