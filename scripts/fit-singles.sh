#!/bin/bash

trial_group () {
    tid=$(echo "$1" | grep -oP '\d{3}(?=\d{5}/Pass.*$)')
    if [ $(echo "$tid" | grep -oP '^\d') -eq 1 ]; then age="young"; else age="old"; fi
    if [ $(echo "$tid" | grep -oP '\d$') -eq 1 ]; then gender="female"; else gender="male"; fi
    
    echo "$age-$gender"
}


if ! [ -d "$1/Ver_A" ] || ! [ -d "$1/Ver_B" ]; then
    echo "Did not find Ver_A and Ver_B in $1."
    exit 1;
fi

if [ -z "$2" ]; then
    TMP=".logs"
else
    TMP=$2
fi
if [ -d "$TMP" ]; then
    rm -r "$TMP"
fi
mkdir -p "$TMP"

MODELS="ModelTD ModelFP ModelQLA"
MAXITERATIONS=5000
MINIMPROVEMENT=1e-4
LR=0.05
RANDOMTRIES=4

for model in $MODELS; do
    logs=""

    for subject in "$1"/Ver_A/*/Pass*; do
        (>&2 echo "fitting $model to $subject")
        group=$(trial_group "$subject")
        if ! [ -d "$TMP/$group" ]; then mkdir "$TMP/$group"; fi
        logfile="$TMP/$group/$model"-$(basename "$subject").log
        logs="$logs $logfile"
        
        tail -n+27 "$subject" | python3 scripts/tables-to-histories.py A | cabal exec maze-fit -- - $model --learningrate $LR -i $MAXITERATIONS --improvement $MINIMPROVEMENT | tail -n+2 >> "$logfile"
        for i in $(seq $RANDOMTRIES); do
            tail -n+27 "$subject" | python3 scripts/tables-to-histories.py A | cabal exec maze-fit -- - $model --learningrate $LR -i $MAXITERATIONS --improvement $MINIMPROVEMENT --random $i | tail -n+2 >> "$logfile"
        done
    done

    for subject in "$1"/Ver_B/*/Pass*; do
        (>&2 echo "fitting $model to $subject")
        group=$(trial_group "$subject")
        if ! [ -d "$TMP/$group" ]; then mkdir "$TMP/$group"; fi
        logfile="$TMP/$group/$model"-$(basename "$subject").log
        logs="$logs $logfile"

        tail -n+27 "$subject" | python3 scripts/tables-to-histories.py B | cabal exec maze-fit -- - $model --learningrate $LR -i $MAXITERATIONS --improvement $MINIMPROVEMENT | tail -n+2 >> "$logfile"
        for i in $(seq $RANDOMTRIES); do
            tail -n+27 "$subject" | python3 scripts/tables-to-histories.py B | cabal exec maze-fit -- - $model --learningrate $LR -i $MAXITERATIONS --improvement $MINIMPROVEMENT --random $i | tail -n+2 >> "$logfile"
        done
    done

    python3 scripts/fitting-stats.py table $(echo "$logs" | grep -oP '[^\s]*/young-[^\s]*')
    python3 scripts/fitting-stats.py table $(echo "$logs" | grep -oP '[^\s]*/old-[^\s]*')
done