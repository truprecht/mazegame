""""""
from sys import argv, stdin
from maze.history import tobehav

TABLE_COLS = ["jump", "at_door", "at_room", "target_room"] \
             + ["exit_%s"%(_dir,) for _dir in "NESW"] \
             + ["decision_rt", "timeout", "reward_condition", "sum_reward", "steps"] \
             + ["door_%d" %(_i,) for _i in range(24)]

if __name__ == "__main__":
    assert len(argv) < 3, "Use %s [<HISTORY FILE>]." %(argv[0], )

    print("\t".join(TABLE_COLS))

    if len(argv) == 1:
        print("\n".join(tobehav(stdin.read())))

    elif len(argv) == 2:
        with open(argv[1]) as histfile:
            print("\n".join(tobehav(histfile.read())))
    