from sys import argv
from random import choice
from maze.presets import PRESETS
from maze.history import dump_doors, dump_edge

def jump_sequence(destinations, steps):
    jumps = []
    destinations_ = list(destinations)
    for _ in range(steps):
        if destinations_ and choice(([False] * 9) + [True]):
            ((x,y), direction), destinations_ = destinations_[0], destinations_[1:]
            jumps.append("Just ((%d,%d),%s)" % (x,y,direction))
        else:
            jumps.append("Nothing")
    return ",".join(jumps)

if __name__ == "__main__":
    assert len(argv) == 2 and argv[1] in ['A', 'B']
    presets = PRESETS[argv[1]]

    structure = "Game {gameStep = StepStart, gameMaze = (\"%s\",%s), gamePos = %s, gameReward = Nothing, gameStopSteps = 500, gameTotalSteps = 0, warps = [%s], doorFlips = [%s]}"
    
    print(structure %( dump_doors(presets["MAZE"])
                     , presets["REWARDS"]
                     , "((%d,%d),%s)" %(presets["START"][0] + (presets["START"][1],))
                     , jump_sequence(presets["WARPS"], 500)
                     , ",".join([ "[%s]" %(",".join([dump_edge(flip) for flip in fliplist]), ) for fliplist in presets["FLIPS"]])
                     ))