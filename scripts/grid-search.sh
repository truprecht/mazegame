#!/bin/bash

for discount in {0..9}; do
    for doordecay in {0..9}; do
        for doorrate in {0..9}; do
            for valuerate in {0..9}; do
                for valueinit in {0..9}; do
                    cabal exec maze-sim -- ModelQLA --simulation-seed=0 -g example.maze -p "discount=0.${discount} door-decay=0.${doordecay} door-rate=0.${doorrate} value-rate=0.${valuerate} value-init=0.${valueinit}"
                done
            done
        done
    done
done