from maze.presets import PRESETS
from re import findall, match

def door_to_coord(nr):
    if nr < 0 or nr > 23:
        raise Exception("%d is not a valid door." %(nr, ))
    
    col = int(nr / 7)
    row_ = nr % 7
    if row_ < 3:
        direction = 'N'
        row = row_
    else:
        direction = 'E'
        row = row_ - 3
    return ((col + 1, row + 1), direction)

def coord_to_door(ccd):
    ((col, row), direction) = ccd
    
    # normalize
    if direction == 'W': 
        direction = 'E'
        col -= 1
    if direction == 'S':
        direction = 'N'
        row -= 1

    dn = (col - 1) * 7 + row - 1 + (0 if direction == 'N' else 3)
    assert dn >= 0 and dn <= 23, "Got invalid door %s [%d]." %(str(ccd), dn)
    
    return dn

def room_to_coord(nr):
    if nr < 0 or nr > 15:
        raise Exception("%d is not a valid room." %(nr, ))
    
    col = nr % 4
    row = int(nr / 4)
    return (col + 1, row + 1)

def coord_to_room(cc):
    (col, row) = cc
    
    rn = (row - 1) * 4 + col - 1
    assert rn >= 0 and rn <= 15, "Got invalid room %s [%d]." %(str(cc), rn)

    return rn

def global_direction(room, door):
    door_coord, door_dir = door
    if door_coord == room:
        return door_dir
    elif door_coord[0] == room[0]:
        if door_coord[1] == room[1] - 1 and door_dir == 'N':
            return 'S'
        if room[1] == door_coord[1] - 1 and door_dir == 'S':
            return 'N'
    elif door_coord[1] == room[1]:
        if door_coord[0] == room[0] - 1 and door_dir == 'E':
            return 'W'
        if room[0] == door_coord[0] - 1 and door_dir == 'W':
            return 'E'
    raise Exception("Door %s is not a part of room %s." %(str(door), str(room)))

def flip_dir(direction):
    if direction == 'E': return 'W'
    if direction == 'W': return 'E'
    if direction == 'N': return 'S'
    if direction == 'S': return 'N'
    raise Exception("%s is not a valid direction." %(direction, ))

def cicle(direction, i):
    i = i % 4
    if i == 0: return direction
    else:
        if direction == 'N': return cicle('E', i-1)
        if direction == 'E': return cicle('S', i-1)
        if direction == 'S': return cicle('W', i-1)
        if direction == 'W': return cicle('N', i-1)
    raise Exception("%s is not a valid direction." %(direction, ))
 

def walk(room, direction):
    x,y = room
    if direction == 'N': return (x,y+1)
    if direction == 'S': return (x,y-1)
    if direction == 'E': return (x+1,y)
    if direction == 'W': return (x-1,y)
    raise Exception("%s is not a valid direction." %(direction, ))

def door_dir(door, fdir):
    ((x,y), ddir) = door_to_coord(door)
    if fdir == 1:
        return ((x, y+1), 'N')
    if fdir == 2:
        return ((x, y), 'W')
    if fdir == 3:
        return ((x, y), 'S')
    if fdir == 4:
        return ((x+1, y), 'E')


def recover_direction(prevroom, currentroom):
    x1, y1 = prevroom
    x2, y2 = currentroom

    if x1 == x2:
        if y2 == (y1 - 1): return ('S')
        elif y2 == (y1 + 1): return ('N')
    if y1 == y2:
        if x2 == (x1 - 1): return ('W')
        elif x2 == (x1 + 1): return ('E')
    
    raise Exception("Cannot recover step from %s to %s." %(prevroom, currentroom))

def key_from_directions(prev_dir, next_dir):
    prev_dir = flip_dir(prev_dir)
    for i in range(1, 4):
        if cicle(prev_dir, i) == next_dir:
            return i
    
    raise Exception("Cannot infer pressed key from %s -> %s." %(prev_dir, next_dir))


def tohist(history, preset):
    results = []

    doors = PRESETS[preset]["MAZE"]
    rewards = PRESETS[preset]["REWARDS"] 
    chosenStep = "StepStart"
    actualStep = "StepStart"
    currentReward = "Nothing"
    skip = False

    for linenr in range(len(history)):
        line = history[linenr].split()
        index = line[0]
        jump = line[6] 
        at_door, at_room = line[9:11]
        target_room = line[13]
        key = line[20]
        timeout = line[22]
        reward = line[23]
        doorflips = map(lambda x: bool(int(x)), line[-24:])

        room = room_to_coord(int(at_room))
        try:
            direction = global_direction(room, door_to_coord(int(at_door)))
        except Exception as e:
            try:
                # try to repair multiple key presses
                # we consider the logged key and door wrong
                if not skip:
                    if actualStep != "StepWarp":
                        # recover last step, direction should be the same
                        assert chosenStep == actualStep, "Steps %s and %s do not match." %(chosenStep, actualStep)
                        (x, y) = findall(r"""\((\d),(\d)\)""", results[-1])[-1]
                        
                        stepdir = recover_direction( (int(x), int(y)), room )
                        chosenStep = actualStep = "(StepDir %s)" %(stepdir,)
                        direction = flip_dir(stepdir)
                    else:
                        # recover direction using next action and pressed key,
                        # cannot recover the chosen action, though
                        nextroom = room_to_coord(int(history[linenr+1].split()[10]))
                        direction = cicle(recover_direction(room, nextroom), -int(key))
                else:
                    # if timeout occurred, use last valid direction
                    pass
            except Exception as e_:
                raise Exception("error at index %d:\n\t%s\n\t%s" %(int(index) , str(e), str(e_)))
        
        if not skip:
            results.append("S %f %s %f %s (\"%s\",%s) (%d,%d) %s %s" %(0, chosenStep, 0, actualStep, dump_doors(doors), rewards, room[0], room[1], flip_dir(direction), currentReward))
        skip = False
        
        if timeout != '1':
            decisionDir = cicle(direction, int(key))
            chosenStep = "(StepDir %s)" %(decisionDir,)
            actualStep = "(StepDir %s)" %(decisionDir,) if jump == '0' else "StepWarp"
            currentReward = "Nothing" if float(reward) == 0 else "(Just %f)" %(float(reward) / 5)
        else:
            skip = True

        doors = [state != flip for (state, flip) in zip(doors, doorflips)]

    results.append("S %f %s %f StepStop (\"%s\",%s) (%d,%d) %s %s" %(0, chosenStep, 0, dump_doors(doors), rewards, 0, 0, flip_dir(direction), currentReward))
    return results


def tobehav(history):
    results = []

    doors = []
    jump = False
    steps = 0
    rewards = 0
    for histline in findall(r"S (\d\.\d+) (StepStart|\(StepDir [NSWE]\)) (\d\.\d+) (StepStart|\(StepDir [NSWE]\)|StepWarp|StepStop) \(\"([+-]{24})\",\[[^\]]+\]\) \((\d),(\d)\) ([NSEW]) (\(Just \d\.\d+\)|Nothing)", history):
        (t_zero, chosen_step, t_key, actual_step, maze_dump, room_x, room_y, direction, reward) = histline
        
        switches = [ prevdoor != currentdoor for (prevdoor, currentdoor) in zip(doors, doors_from_dump(maze_dump)) ]
        doors = doors_from_dump(maze_dump)

        pos = (int(room_x), int(room_y))

        historystep = match(r"""\(StepDir ([NSWE])\)""", chosen_step)
        if historystep:
            walking_dir = historystep.group(1)
            exits = []
            for _dir in "NESW":
                try: exits.append(doors[coord_to_door(((last_pos, _dir)))] if _dir in "NE" else not doors[coord_to_door(((last_pos, _dir)))])
                except: exits.append(False)
            
            results.append( "%d\t%d\t%d\t%d\t%s\t%f\t%d\t%d\t%d\t%d\t%s"
                %( jump
                , coord_to_door((last_pos, flip_dir(last_direction)))
                , coord_to_room(last_pos)
                , coord_to_room(walk(last_pos, walking_dir))
                , "\t".join(['1' if e else '0' for e in exits])
                , (float(t_key) - float(t_zero)) * 1000
                , 0
                , last_reward if last_reward else 0
                , rewards
                , steps
                , "\t".join(['1' if switch else '0' for switch in switches])
                ))
        
        jump = False
        if actual_step == "StepWarp":
            jump = True
        last_direction = direction
        last_pos = pos
        last_reward = match("\(Just (\d\.\d+)\)", reward)
        if last_reward:
            last_reward = int(float(last_reward.group(1)) * 5)
            steps = 0
            rewards += last_reward
        steps += 1

    return results


def dump_doors(arr):
    reordered = arr[0:3] + arr[7:10] + arr[14:17] + arr[21:24] + arr[3:7] + arr[10:14] + arr[17:21]
    door_str = ""
    for is_open in reordered:
        door_str += '+' if is_open else '-'
    return door_str

def doors_from_dump(string):
    reordered = string[0:3] + string[12:16] + string[3:6] + string[16:20] + string[6:9] + string[20:24] + string[9:12]
    return [ True if door == '+' else False for door in reordered ]

def dump_edge(e):
    ((x, y), direction) = e
    return "Edge {edgeR = (%d,%d), edgeD = %s}" % (x,y,direction)