""""""
from sys import argv, stdin
from maze.history import tohist

def format_nested_array(arrarr):
    return "[\t%s\n]" %("\n,\t".join( \
            ["[\t%s\n\t]" %("\n\t,\t".join(innerarr),) for innerarr in arrarr] \
        ),)

if __name__ == "__main__":
    assert len(argv) > 1 and argv[1] in ['A', 'B'], \
        "Use %s (A|B) [<TABLE FILE 1> <TABLE FILE 2> ...]" %(argv[0], )
    PRESET, TABLES = argv[1], argv[2:]

    if not TABLES:
        print(format_nested_array([tohist(stdin.readlines(), PRESET)]))

    else:
        print(format_nested_array([tohist(open(table).readlines(), PRESET) for table in TABLES]))
