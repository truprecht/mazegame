from sys import argv, stdin
from json import loads

def fromSV(string):
    return { k: float(v)  for (k, v) in [ kv.split("=") for kv in string.split() ] }

def sequence(lod):
    sequenced = {}
    for d in lod:
        for key in d:
            try: sequenced[key].append(d[key])
            except: sequenced[key] = [d[key]]
    return sequenced

def median(lov):
    assert len(lov) > 0
    return sorted(lov)[int(len(lov) / 2)]

def print_table(table):
    attrspace = max([ len(k) for k in table ])
    spaces = []

    # header
    row = " " * attrspace
    for label in table["label"]:
        space = max(len(label), 8)
        spaces.append(space)
        row += (" | {:<" + str(space) + "}").format(label)
    print(row)

    # separation
    row = "=" * (attrspace + sum(spaces) + 3*len(spaces))
    print(row)

    # rows
    for attr in table:
        if attr == "label": continue
        row = ("{:<" + str(attrspace) + "}").format(attr)
        
        for space, value in zip(spaces, table[attr]):
            floating = min(8, space)
            row += (" | {:>" + str(space) + "." + str(floating) + "}").format(("{:." + str(floating-2) + "f}").format(float(value)))
        print(row)

def print_csv(table):
    # header
    row = ""
    for attr in table["label"]:
        row += ", %s" %(attr)
    print(row)
    
    # rows
    for attr in table:
        if attr == "label": continue
        row = attr
        for value in table[attr]:
            row += ", %s" %(value)
        print(row)


if __name__ == "__main__":
    assert len(argv) > 2 and argv[1] in ["table", "csv"], \
        "Use %s MODE FILE1 FILE2 ...\nwhere MODE is either 'table' or 'csv'." %(argv[0])

    listOfFits = []
    for fittingFile in argv[2:]:
        label = fittingFile.split("/")[-1]
        with open(fittingFile) as contents:
            lines = contents.readlines()
            lastFit, random, likelihood, bic, rhosqared = min([line.strip().split("\t") for line in lines if line.strip()], key=lambda x: float(x[2]))
            lastFit = fromSV(lastFit.replace("{", "").replace("}", ""))
            lastFit["likelihood"], lastFit["bic"], lastFit["rhosquared"], lastFit["random"], lastFit["label"] = float(likelihood), float(bic), float(rhosqared), float(random), label
            listOfFits.append(lastFit)
    
    medianScores = { "label": "median" }
    for k, l in sequence(listOfFits).items():
        if not k == "label":
            medianScores[k] = median(l)
    listOfFits.append(medianScores)

    if argv[1] == "table":
        print_table(sequence(listOfFits))
    elif argv[1] == "csv":
        print_csv(sequence(listOfFits))